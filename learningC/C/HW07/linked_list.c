#include "linked_list.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MEM_ERROR_MESSAGE "Error: Chyba paměti!\n"
#define BAD_INP_ERROR_MESSAGE "Error: Chybny vstup!\n"
#define POINTER_SIZE sizeof(int *)

typedef struct list_item
{

    struct list_item *next;
    struct list_item *prev;

    int value;

} list_item;

typedef struct global_list
{

    list_item *start;
    list_item *end;
    int num_of_items;

} global_list;

global_list list = {NULL, NULL, 0};

void *mallocate(size_t size);

list_item *create_item(list_item *prev, list_item *next, int value);

void *mallocate(size_t size)
{
    /**
     * @brief Tries to allocate memory of given size, in case of failure terminates program
     *
     */
    void *ret = malloc(size);
    if (!ret)
    {
        fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
        exit(-1);
    }
    return ret;
}

list_item *create_item(list_item *prev, list_item *next, int value)
{

    list_item *item = mallocate(sizeof(list_item));

    item->value = value;
    item->next = next;
    item->prev = prev;

    return item;
}

_Bool push(int entry)
{
    list_item *curr_end = list.end;
    list_item *new_item = create_item(curr_end, NULL, entry);
    if (curr_end == NULL)
    {
        list.end = list.start = new_item;
    }
    else
    {
        list.end = new_item;
        curr_end->next = new_item;
    }
    list.num_of_items++;
    return true;
}

_Bool insert(int entry)
{

    list_item *curr_item = list.start;

    // if it is bigger than first value, puts entry to the first place
    //(or in case there are no items in list)
    if (curr_item == NULL || curr_item->value <= entry)
    {
        list_item *new_item = create_item(NULL, curr_item, entry);
        list.start = new_item;
        if (curr_item != NULL)
        {
            curr_item->prev = new_item;
        }
        else
        {
            list.end = new_item;
        }
        list.num_of_items++;
        return true;
    }

    while (true)
    {
        list_item *next_item = curr_item->next;

        if (next_item == NULL || next_item->value <= entry)
        {
            list_item *new_item = create_item(curr_item, next_item, entry);

            curr_item->next = new_item;
            // inserting inside list
            if (next_item != NULL)
            {
                next_item->prev = new_item;
            }
            // inserting to the end of list
            else
            {
                list.end = new_item;
            }

            list.num_of_items++;
            return true;
        }
        curr_item = next_item;
    }

    return false;
}

int pop(void)
{
    if (list.num_of_items > 0)
    {
        list_item *new_start = list.start->next;

        int val = list.start->value;

        if (list.start == list.end)
        {
            list.end = NULL;
        }

        free(list.start);
        if (new_start != NULL)
        {
            new_start->prev = NULL;
        }

        list.start = new_start;
        list.num_of_items--;
        return val;
    }
    // no items in list
    return -1;
}

int size(void)
{
    return list.num_of_items;
}

_Bool erase(int entry)
{
    list_item *curr_item = list.start;
    bool found_value = false;
    while (curr_item != NULL)
    {
        if (curr_item->value == entry)
        {
            list_item *prev_item = curr_item->prev;
            list_item *next_item = curr_item->next;

            // reconnect list
            if (prev_item != NULL)
            {
                prev_item->next = next_item;
            }
            else
            {
                list.start = next_item;
            }
            if (next_item != NULL)
            {
                next_item->prev = prev_item;
            }
            else
            {
                list.end = prev_item;
            }

            found_value = true;
            free(curr_item);
            curr_item = next_item;
            list.num_of_items--;
        }
        else
        {
            curr_item = curr_item->next;
        }
    }

    return found_value;
}

int getEntry(int idx)
{
    int value = -1;

    if (idx >= 0 && idx < list.num_of_items)
    {
        // cycle through list
        list_item *curr = list.start;
        for (int i = 0; i < idx; i++)
        {
            curr = curr->next;
        }
        value = curr->value;
    }

    return value;
}

void clear()
{
    list_item *curr_item = list.start;

    // free all remaining items in list
    while (curr_item != NULL)
    {
        list_item *next_item = curr_item->next;
        free(curr_item);
        curr_item = next_item;
    }

    // reinit values of list
    list.num_of_items = 0;
    list.end = list.start = NULL;
}