#include "queue.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MEM_ERROR_MESSAGE "Error: Chyba paměti!\n"
#define BAD_INP_ERROR_MESSAGE "Error: Chybny vstup!\n"
#define POINTER_SIZE sizeof(int *)

typedef int (*compare_ptr)(const void *, const void *);
typedef void (*clear_ptr)(void *);

typedef struct list_item
{

    struct list_item *next;
    struct list_item *prev;

    void *value;

} list_item;

typedef struct global_queue
{

    list_item *start;
    list_item *end;

    compare_ptr comptr;
    clear_ptr clearptr;

    int num_of_items;

} global_queue;

// declaration of additional functions(normally i would declare them in header file, but I cant this time)
void *mallocate(size_t size);
list_item *create_item(list_item *prev, list_item *next, void *value);
void free_item(list_item *item, global_queue *queue);

void *mallocate(size_t size)
{
    /**
     * @brief Tries to allocate memory of given size, in case of failure terminates program
     *
     */
    void *ret = malloc(size);
    if (!ret)
    {
        fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
        exit(-1);
    }
    return ret;
}

list_item *create_item(list_item *prev, list_item *next, void *value)
{

    list_item *item = mallocate(sizeof(list_item));
    item->value = value;
    item->next = next;
    item->prev = prev;

    return item;
}

void *create()
{
    /**
     * @brief creates a global_queue structure and inicializes all vars to NULL/zero
     *
     */
    global_queue *ret = mallocate(sizeof(global_queue));
    ret->end = NULL;
    ret->start = NULL;
    ret->num_of_items = 0;
    ret->clearptr = NULL;
    ret->comptr = NULL;
    return (void *)ret;
}

// prolly you do not need that
void free_item(list_item *item, global_queue *queue)
{
    /**
     * @brief frees the item struct
     *
     */
    if (item != NULL)
    {

        // in case clear for value set up, use
        if (queue->clearptr != NULL)
        {
            queue->clearptr(item->value);
        }
        else
        {
            free(item->value);
        }
        free(item);
    }
}
void clear(void *queue)
{
    /**
     * @brief dealocates all dynamic memory inside queue
     *
     */

    global_queue *list = (global_queue *)queue;

    while (list->num_of_items > 0)
    {
        list_item *curr = list->start;
        list->start = curr->next;

        // in case clear for value set up, use
        if (list->clearptr != NULL)
        {
            list->clearptr(curr->value);
        }
        free(curr);

        list->num_of_items--;
    }

    list->start = list->end = NULL;
}

_Bool push(void *queue, void *entry)
{
    // in case bad args, return False
    if (queue == NULL || entry == NULL)
    {
        return false;
    }

    global_queue *list = (global_queue *)queue;

    list_item *curr_end = list->end;
    list_item *new_item = create_item(curr_end, NULL, entry);
    if (curr_end == NULL)
    {
        list->end = list->start = new_item;
    }
    else
    {
        list->end = new_item;
        curr_end->next = new_item;
    }
    list->num_of_items++;
    return true;
}

void *pop(void *queue)
{
    global_queue *list = (global_queue *)queue;

    if (list->num_of_items > 0)
    {
        list_item *new_start = list->start->next;

        void *val = list->start->value;

        if (list->start == list->end)
        {
            list->end = NULL;
        }

        free(list->start);
        if (new_start != NULL)
        {
            new_start->prev = NULL;
        }

        list->start = new_start;
        list->num_of_items--;
        return val;
    }

    // no items in list
    return NULL;
}

_Bool insert(void *queue, void *entry)
{
    global_queue *list = (global_queue *)queue;
    list_item *curr_item = list->start;

    // if it is bigger than first value, puts entry to the first place
    //(or in case there are no items in list)
    if (curr_item == NULL || list->comptr(entry, curr_item->value) >= 0)
    {
        list_item *new_item = create_item(NULL, curr_item, entry);
        list->start = new_item;
        if (curr_item != NULL)
        {
            curr_item->prev = new_item;
        }
        else
        {
            list->end = new_item;
        }
        list->num_of_items++;
        return true;
    }

    while (true)
    {
        list_item *next_item = curr_item->next;

        if (next_item == NULL || list->comptr(entry, next_item->value) >= 0)
        {
            list_item *new_item = create_item(curr_item, next_item, entry);

            curr_item->next = new_item;
            // inserting inside list
            if (next_item != NULL)
            {
                next_item->prev = new_item;
            }
            // inserting to the end of list
            else
            {
                list->end = new_item;
            }

            list->num_of_items++;
            return true;
        }
        curr_item = next_item;
    }

    return false;
}

_Bool erase(void *queue, void *entry)
{
    global_queue *list = (global_queue *)queue;

    list_item *curr_item = list->start;
    bool found_value = false;

    while (curr_item != NULL)
    {
        if (list->comptr(entry, curr_item->value) == 0)
        {
            list_item *prev_item = curr_item->prev;
            list_item *next_item = curr_item->next;

            // reconnect list
            if (prev_item != NULL)
            {
                prev_item->next = next_item;
            }
            else
            {
                list->start = next_item;
            }
            if (next_item != NULL)
            {
                next_item->prev = prev_item;
            }
            else
            {
                list->end = prev_item;
            }

            found_value = true;
            free_item(curr_item, list);
            curr_item = next_item;
            list->num_of_items--;
        }
        else
        {
            curr_item = curr_item->next;
        }
    }

    return found_value;
}

void *getEntry(const void *queue, int idx)
{
    global_queue *list = (global_queue *)queue;

    // in case bad args return NULL
    if (queue == NULL || idx < 0 || idx >= list->num_of_items)
    {
        return NULL;
    }

    // cycle through list
    list_item *curr = list->start;
    for (int i = 0; i < idx; i++)
    {
        curr = curr->next;
    }
    return curr->value;
}

int size(const void *queue)
{
    global_queue *list = (global_queue *)queue;
    return list->num_of_items;
}

void setCompare(void *queue, int (*compare)(const void *, const void *))
{
    global_queue *list = (global_queue *)queue;
    list->comptr = compare;
}

void setClear(void *queue, void (*clear)(void *))
{
    global_queue *list = (global_queue *)queue;
    list->clearptr = clear;
}
