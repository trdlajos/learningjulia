
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//for string variable, defines maximal length of our string
#define ARR_LEN 20

enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INPUT = 100,
    ERROR_INPUT_OUT_OF_RANGE,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Chybna delka vstupu!\n"
};
void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_BAD_INPUT && error <= ERROR_INPUT_OUT_OF_RANGE) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INPUT]):fprintf(stderr,"%s",error_strings[0]);
  
}

void print_specific_index(int array[],int index){
  printf("Value on index: %d is %d\n",index,array[index]);
}


void print_array(int array[],int length){
  for (int i = 0; i < length; i++){
    print_specific_index(array,i);
  }
}


int main(int argc, char *argv[]){
    
    int error = 0;
    // designated initializer
    int array[ARR_LEN] = {[4]=5};
    print_array(array,ARR_LEN);
    print_specific_index(array,4);

    // pointers 
    printf("\nNow it is just about pointers\n");
    int value = 10;
    int *p = &value;
    printf("Printing value adress: %p\n",p);
    printf("Printing value: %d\n",*p);
    *p = 30;
    printf("Printing new value: %d\n",value);

    // pointer pointing to pointer

    int **pp = &p;
    printf("Adress of pointer to pointer: %p\n",pp);
    printf("Value of pointer that is pointed to by pointer to pointer: %p\n",*pp);
    printf("Printing value of pointer that is pointed to by pointer to pointer: %d\n",**pp);
  
    printf("\nArrays and pointers\n");

    int arr[10] = {[5]=5, [6]=6};
    // note that name of array itself works as pointer!
    int *ap = &arr[0];
    printf("Value of array on fifth place: %d\n",*(ap+5));
    int *ap_two = &arr[6];
    printf("Distance between two pointers: %ld\n",ap_two-ap);

    printf("\nMultidimensional arrays\n");

    int multi_arr[10][10] = {[1][2] = 10, [9][8] = 17};
    //treating as one dim array:
    int *one_p = &multi_arr[0][0];
    printf("Printing value of pointer: %d\n",*(one_p+12));
    int *row_p = &multi_arr[9][0];
    printf("Printing value of pointer: %d\n",*(row_p+8));
    
  



    return error;
}