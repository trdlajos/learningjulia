#include <stdio.h>
#include <stdlib.h>

/**
 * @brief info 
 * stream == source of input or destination of output
 * 
 * File pointers
 *  examples:
    *  stdin
    *  stdout
    *  stderr
 *
 * many IO funcs use these standart IOs, but can be redirected
 * example:  
 *  input/output redirection via command line
 *  demo < in.dat
 * 
 * two basic file types - binary and text
 * 
 * must be opened and closed!
 * close in case of failure returns EOF error code
 * there is a buffer between file and IO, that stores values, so they can be accesed more easily
 * 
 * Text files 
 *  have special EOF markers
 *  have special end of line characters
 *  
 * Binary files
 *  in mode, there should be b as declaration we work with binary file
 */

/* The main program */
#include <stdio.h>
#include <stdlib.h>


#define BASIC_STR_LEN 10

// error codes inicialization
enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_NO_MATCH = 1,
  ERROR_FILE_NOT_FOUND = 100,
  ERROR_NO_ARG,

};

// error messages inicialization
static const char *error_strings[] = {
    "Error: File not found!\n",
    "Error: Arguments not specified!\n",

};

void report_error(int error);
char *read_message(int *length, int *error);
int open_file(FILE *file_ptr, char *file_name);
int control(char *pattern,char *file_name);

/* The main program */
int main(int argc, char *argv[])
{
  int error = ERROR_OK;
  if (argc > 2)
  {
    char *pattern = argv[1];
    char *file_name = argv[2];
  
    // debug
    printf("%s\n%s\n",pattern,file_name);
    error = control(pattern,file_name);
  }
  else{
    error = ERROR_NO_ARG;
  }
  printf("%d",error);
  return error;
}

int control(char *pattern,char *file_name){
  int error = ERROR_OK;
  
  //open file
  FILE *file_ptr = fopen(file_name,"r");
  if(file_ptr == NULL){
    return ERROR_FILE_NOT_FOUND;
  }
  

  char test[25];
  char test_two[10];
  
  //read line from file
  fgets(test,20,file_ptr);
  fgets(test_two,10,file_ptr);
  printf("%s\n%s\n",test,test_two);
  return error;

}


