/**
 * @file string.c
 * @author JT
 * @brief Serves as introduction to string literals for the author
 * @version X.X
 * @date 2023-03-24
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//for string variable, defines maximal length of our string
#define STR_LEN 100

enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INPUT = 100,
    ERROR_INPUT_OUT_OF_RANGE,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Chybna delka vstupu!\n"
};

int read_line(char ch[],int length){
    /**
     * @brief reads from stdin one line(termined by \n, but \n is not stored), returns success if reading ok, error if reading failure
     * 
     */
    int i = 0;
    short check = 0;

    for(;i<length;i++){
        ch[i]=getchar();
        if (ch[i]=='\n'){
            check = 1;
            break;
        }
    }
    
    while(i<length){
        ch[i]='\0';
        i++;
    }

    if(check)
        return ERROR_OK;
    return ERROR_INPUT_OUT_OF_RANGE;
}

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_BAD_INPUT && error <= ERROR_INPUT_OUT_OF_RANGE) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INPUT]):fprintf(stderr,"%s",error_strings[0]);
  
}

int count_spaces(const char *s){
    /**
     * @brief counts number of spaces in string s
     * 
     */
    int number_of_spaces = 0;
    while(*s != '\0'){
        if(*s == ' ')
            number_of_spaces++;
        s++;
    }
    return number_of_spaces;
}


int main(){

    //pointer to start of the string, note that you cant modify stored string
    char *string_pointer_p;
    string_pointer_p = "p points  to the first letter in this string";
    printf("%s",string_pointer_p);
    
    //string variables !NOTE that you will always need one place to store null literal, marking the end of string
    char str[STR_LEN+1] = "From the end of this message, every other place in this array will contain NULL character";  // making sure there is +1 space for NULL character
    printf("\n%.5s",str);

    //it is possible to omit length of str completely, it will be computed for us 
    char str_computed[] = "From the end of this message, every other place in this array will contain NULL character";  // making sure there is +1 space for NULL character
    printf("\n%s",str_computed);

    // note that every function expecting pointer to string as argument will work for both variants 
    // BUT there is difference! Array can be modified, pointer can be made to point elsewhere

    putchar('\n');

    //note that scaning with scanf will only work between two whitespace characters
    char str_read[STR_LEN+1];
    scanf("\n%s",str_read);
    printf("\nscanf: %s\n",str_read);


    //gets behaves more developer friendly, note that in this compositon, scanf will get first word and fgets the rest
    fgets(str_read,STR_LEN,stdin);
    printf("\nfgets: %s",str_read);


    putchar('\n');
    putchar('M');
    putchar('S');
    putchar('\n');
    
    char my_string[STR_LEN+1];
    int error = read_line(my_string,STR_LEN+1);
    report_error(error);
    printf("my string: %s\n",my_string);

    // counts number of whitespaces in string
    printf("Spaces num: %d\n", count_spaces(my_string));
    
    //string functions in string library
    printf("\nSTRING FUNCS\n");
    
    printf("Copying\n");
    //note that it does not check length of arrays
    char new_string[STR_LEN+1];
    strcpy(new_string,"This string has been copied");
    printf("new string: %s\n",new_string);
    putchar('\n');

    printf("length\n");
    // measures length of string stored in array, no array itself!
    printf("%d\n",(int)strlen(my_string));
    putchar('\n');

    printf("string concatenation\n");
    // joins two strings together, stores in first string, note that in case of overflow behavior not defined!, there is safer version called strncat! 
    strcat(my_string," + this");
    printf("%s\n",my_string);
    putchar('\n');

    printf("string comparison\n");
    // compares two strings, 0 means they are same
    printf("Comparison: %d\n",strcmp("abc","abd"));
    putchar('\n');
    
    


    return error;
}