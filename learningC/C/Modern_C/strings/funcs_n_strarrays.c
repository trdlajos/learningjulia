/**
 * @file my_funcs.c
 * @author JT
 * @brief Serves as place for implementation of various string functions for better understatement of C
 * @version X.X
 * @date 2023-03-24
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//for string variable, defines maximal length of our string
#define STR_LEN 100

enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INPUT = 100,
    ERROR_INPUT_OUT_OF_RANGE,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Chybna delka vstupu!\n"
};

int read_line(char ch[],int length){
    /**
     * @brief reads from stdin one line(termined by \n, but \n is not stored), returns success if reading ok, error if reading failure
     * 
     */
    int i = 0;
    short check = 0;

    for(;i<length;i++){
        ch[i]=getchar();
        if (ch[i]=='\n'){
            check = 1;
            break;
        }
    }
    
    while(i<length){
        ch[i]='\0';
        i++;
    }

    if(check)
        return ERROR_OK;
    return ERROR_INPUT_OUT_OF_RANGE;
}

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_BAD_INPUT && error <= ERROR_INPUT_OUT_OF_RANGE) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INPUT]):fprintf(stderr,"%s",error_strings[0]);
  
}

int count_spaces(const char *s){
    /**
     * @brief counts number of spaces in string s
     * 
     */
    int number_of_spaces = 0;
    while(*s){
        if(*s == ' ')
            number_of_spaces++;
        s++;
    }
    return number_of_spaces;
}

int meassure_len(const char *s){
    /**
     * @brief meassures len of string s
     * 
     */
    const char *p = s;
    while(*s){
        s++;
    }

    return s-p;
}

char *copy_string(char *p,const char *c){
    char *start = p;
    while(*p){
        p++;
    }
    while(*c){
        *p = *c;
        p++;
        c++;
    }

    return start;

}

int main(int argc, char *argv[]){
    // argc - argument count, argv - arguments vectors , index 0 stands for name of this file
    
    int error = 0;

    // length meassurement
    char my_string[STR_LEN+1];
    read_line(my_string,STR_LEN+1);
    printf("Length: %d\n", meassure_len(my_string));
    
    //white space meassurement
    printf("White spaces: %d\n", count_spaces(my_string));
    
    // copy string 
    
    printf("Copying... %s\n",copy_string(my_string," + add this"));
    printf("%s\n",my_string);

    // best way to store strings in array : array of pointers to strings
    char *planets[] = {"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uran","Neptune"};
    printf("%s\n",planets[0]);
    printf("%c\n",planets[0][0]);
    printf("%s\n",planets[1]);
    printf("Length: %d\n", meassure_len(planets[2]));

    // setting up a pointer to first place in planets
    char *p = planets[0];
    printf("%s\n",p+3);

    

    return error;
}