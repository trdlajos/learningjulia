#include <stdio.h>
#include <stdlib.h>


enum {
  ERROR_OK = EXIT_SUCCESS,
  ERROR_BAD_INP = 100,
  ERROR_BAD_INTERVAL = 101,
  ERROR_NOT_EVEN = 102
};

static const char *error_strings[] = {
  "ERROR: Chybny vstup!",
  "ERROR: Vstup mimo interval!",
  "ERROR: Sirka neni liche cislo!"
};

void report_error_switch(int error);
void report_error_array(int error);

/* The main program */
int main(int argc, char *argv[])
{
  int ret = 123;
  
  if(ret == ERROR_OK)
  {
    //SUCCESS
  }
  else
  {
    report_error_switch(ret);
    report_error_array(ret);
  }
  
  return ret;
}

void report_error_switch(int error)
{
  switch (error)
  {
  case 100:
    fprintf(stderr,"ERROR: Chybny vstup!");
    break;
  case 101:
    fprintf(stderr,"ERROR: Vstup mimo interval!");
    break;
  case 102:
    fprintf(stderr,"ERROR: Sirka neni liche cislo!");
    break;
  
  default:
    fprintf(stderr,"ERROR: Nedefinovan!");
    break;
  }
}

void report_error_array(int error){
  (error >= ERROR_BAD_INP && error <= ERROR_NOT_EVEN) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INP]):fprintf(stderr,"ERROR: Nedefinovan!");
  
}






