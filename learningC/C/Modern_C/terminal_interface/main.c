/**
 * @file main.c
 * @author JT
 * @brief User interface for terminal: general implementation
 * @version 0.*
 * @date 2023-03-10
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>

// error codes inicialization
enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_UNRECOGNIZED = 100,
    ERROR_BAD_INPUT,
    ERROR_CHOICE_OUT_OF_RANGE,
    ERROR_PROGRAM_FAIL,
};


// error messages inicialization
static const char *error_strings[] = {
    "Error: Error not recognized!\n",
    "Error: Input error!\n",
    "Error: Choice not in range\n",
    "Error: Runtime error\n"
};

// menu messages inicialization
#define BASE_CHOICE 0
#define NUM_OF_CHOICES 3
static const char *menu_strings[] = {
  "0 = exit\n",
  "1 = do_1\n",
  "2 = do_2\n",
  };

void report_error(int error);
int read_input(int *input);
void print_menu();
int decide_action(int input);
void print_array(int array[],int length);
// void print_specific_index(int array[],int index);

int main(){
    //in case you want to use mask for basic variable for better readeability
    typedef int Errors;
    Errors er;
    int input;

    do{
        er = ERROR_OK;
        print_menu();
        er = read_input(&input);
        er == ERROR_OK ? er = decide_action(input):er;
        report_error(er);
    }

    while((er== ERROR_OK || er == ERROR_CHOICE_OUT_OF_RANGE)&&input!=0);
    return er;
}

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_UNRECOGNIZED && error <= ERROR_PROGRAM_FAIL) ? fprintf(stderr,"%s", error_strings[error-ERROR_UNRECOGNIZED]):fprintf(stderr,"%s",error_strings[0]);
  
}

int read_input(int *num){
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   * 
   */
  int buff;
  if(scanf("%d",num)==1){ //note that %2d will read only two digit numbers
    return ERROR_OK;
  }
  else{
    *num = 1;
    return ERROR_BAD_INPUT;
  }
}
void print_menu(){
    for (int i = 0; i <NUM_OF_CHOICES; i++)
    {
        printf("%s",menu_strings[i]);
    }
}

int decide_action(int input){
    switch (input)
        {
        case 0:
            return ERROR_OK;
        case 1:
            putchar('1');
            putchar('\n');
            return ERROR_OK;
        case 2:
            putchar('2');
            putchar('\n');
            return ERROR_OK;
        default:
            return ERROR_CHOICE_OUT_OF_RANGE;
        }
}

void print_array(int array[],int length){
  for (int i = 0; i < length; i++){
    print_specific_index(array,i);
  }
}

void print_specific_index(int array[],int index){
  printf("Value on index: %d is %d\n",index,array[index]);
}
