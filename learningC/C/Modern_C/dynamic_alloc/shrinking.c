/**
 * @file shrinking.c
 * @author JT
 * @brief Simple program that demonstrates working with array of pointers and realloc to smaller dimensions
 * @version 0.1
 * @date 2023-04-16
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#define ARR_LEN 10
#include <stdio.h>
#include <stdlib.h>

#define MEM_ERROR_MESSAGE "Error: Chyba paměti!\n"
#define BAD_INP_ERROR_MESSAGE "Error: Chybny vstup!\n"

enum error
{
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INPUT = 100,
    ERROR_INPUT_OUT_OF_RANGE,
};

void *allocate(int size);
void print_int_array(void **array, int size);
int add_int_to_array(int pos, int value, void **array, int size);
void **allocate_array(int size);

void **reallocate_array(void **array, int new_size);

int main(int argc, char *argv[])
{

    int error = ERROR_OK;
    int size = 8;
    void **array = allocate_array(size);
    add_int_to_array(0, 10, array, size);
    add_int_to_array(7, 10, array, size);
    add_int_to_array(6, 10, array, size);
    add_int_to_array(5, 10, array, size);
    add_int_to_array(3, 10, array, size);
    print_int_array(array, size);
    putchar('\n');
    int new_size = 4;
    array = reallocate_array(array,new_size);
    print_int_array(array,new_size);
    

    return error;
}

void *allocate(int size)
{
    /**
     * @brief Tries to allocate memory of given size, in case of failure terminates program
     *
     */

    void *ret = malloc(size);
    if (!ret)
    {
        fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
        exit(-1);
    }
    return ret;
}

int add_int_to_array(int pos, int value, void **array, int size)
{

    int *value_ptr = (int *)allocate(4);
    *value_ptr = value;

    if (pos >= size)
    {
        printf("INP ERROR");
        return ERROR_INPUT_OUT_OF_RANGE;
    }
    else
    {
        array[pos] = (void *)value_ptr;
        return ERROR_OK;
    }
}

void print_int_array(void **array, int size)
{

    for (int i = 0; i < size; i++)
    {
        if (array[i] == NULL)
        {
            printf("%d: NULL\n", i);
        }
        else
        {

            printf("%d:%d\n", i, *((int *)array[i]));
        }
    }
}

void **allocate_array(int size)
{
    void **array = allocate(8 * size);

    for (int i = 0; i < size; i++)
    {
        array[i] = NULL;
    }

    return array;
}

void **reallocate_array(void **array, int new_size)
{

    void **tmp = realloc(array, new_size * 8);
    if (tmp == NULL)
    {
        free(array);
        printf(MEM_ERROR_MESSAGE);
        exit(-1);
    }
    array = tmp;

    return array;
}
