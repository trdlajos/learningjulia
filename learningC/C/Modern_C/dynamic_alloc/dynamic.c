#define ARR_LEN 10


#include <stdio.h>
#include <stdlib.h>
enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INPUT = 100,
    ERROR_INPUT_OUT_OF_RANGE,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Chybna delka vstupu!\n"
};

int sum(int x);
int quadratic(int x);
int combine_functions(int func_f(int),int (*g)(int),int a);
void print_array(int array[],int length);
void print_specific_index(int array[],int index);
void report_error(int error);



int main(int argc, char *argv[]){
    
    int error = 0;

    int *p;

    // need array of integers!
    p = malloc(ARR_LEN*sizeof(int)); // similar to calloc, but calloc uses size as separate parameter, and initializes all variables to 0
    if (p == NULL)
    {
        printf("Run out of memory!");
        return ERROR_INPUT_OUT_OF_RANGE;
    }
    *p = 5;
    *(p+1) = 10;
    printf("Value: %d\n", *(p+1));
    p[17] = 99;
    printf("Value: %d\n", *(p+17));
    
    
    //reallocate memory
    int *new_p = realloc(p,ARR_LEN*sizeof(int)-4);
    //note that:
        //does not initialize bytes added to the block
        //returns null pointer in case failure
        //if null pointer passed, behaves like malloc
        //0 as size - frees the memory block
        // if shrinks the block down, it will yield same pointer, if it expand, probably will yield new pointer to new block with copied values
    printf("%p\n",p);
    printf("%p\n",new_p);
    printf("Value: %d\n", *(new_p+1));

    // pointers and functions
    printf("%d\n",combine_functions(quadratic,sum,5));

    

  



    return error;
}

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_BAD_INPUT && error <= ERROR_INPUT_OUT_OF_RANGE) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INPUT]):fprintf(stderr,"%s",error_strings[0]);
  
}

void print_specific_index(int array[],int index){
  printf("Value on index: %d is %d\n",index,array[index]);
}


void print_array(int array[],int length){
  for (int i = 0; i < length; i++){
    print_specific_index(array,i);
  }
}

int quadratic(int x){
    return x*x;
}

int sum(int x){
    return x+2;
}

int combine_functions(int func_f(int),int (*g)(int),int a){
    return (*g)((*func_f)(a));
}