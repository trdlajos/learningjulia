#include "queue.h"

void *mallocate(size_t size)
{
    /**
     * @brief Tries to allocate memory of given size, in case of failure terminates program
     *
     */
    void *ret = malloc(size);
    if (!ret)
    {
        fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
        exit(-1);
    }
    return ret;
}

queue_t *create_queue(int capacity)
{
    /**
     * @brief allocates enough space for queue structure and queue array of pointers
     *
     */

    queue_t *queue = mallocate(sizeof(queue_t));
    queue->size = capacity;
    queue->end_pos = queue->first_pos = queue->num_of_items = 0;
    queue->values = mallocate(capacity * POINTER_SIZE);
    queue->has_not_grow = true;

    // init values in queue
    for (int i = 0; i < capacity; i++)
    {
        queue->values[i] = NULL;
    }

    return queue;
}

void delete_queue(queue_t *queue)
{
    /**
     * @brief Frees queue struct and array inside queue
     * 
     */
    if (queue)
    {
        if (queue->values)
        {
            free(queue->values);
        }
        free(queue);
    }
}

bool push_to_queue(queue_t *queue, void *data)
{
    /**
     * @brief pushes an element to the queue, in case queue is full, returns false, else true
     *
     */

    // if full, reallocate
    if (queue->size == queue->num_of_items)
    {
        queue->has_not_grow = false;
        queue = reallocate_queue(queue, queue->size * 2);
    }

    queue->values[queue->end_pos] = data;
    queue->end_pos++;
    queue->num_of_items++;

    // make sure queue is continous
    if (queue->size == queue->end_pos)
    {
        queue->end_pos = 0;
    }
    return true;
}

void *pop_from_queue(queue_t *queue)
{
    /**
     * @brief pops an element from the queue, in case queue is empty, returns NULLS, else void *
     *
     */

    if (0 == queue->num_of_items)
    {
        return NULL;
    }

    void *result = queue->values[queue->first_pos];
    queue->values[queue->first_pos] = NULL;
    queue->first_pos++;
    queue->num_of_items--;

    // shrink down if neccesary
    if (queue->size / 3 > queue->num_of_items && !queue->has_not_grow)
    {
        queue = reallocate_queue(queue, queue->size / 3);
    }

    // make sure queue is continous
    if (queue->size == queue->first_pos)
    {
        queue->first_pos = 0;
    }

    return result;
}

void *get_from_queue(queue_t *queue, int idx)
{
    /**
     * @brief prints item that will be popped after idx iterations
     *
     */
    int pos = queue->first_pos + idx;
    if (pos < 0 || pos > queue->num_of_items + queue->first_pos)
    {
        return NULL;
    }

    if (pos >= queue->size)
    {
        pos = pos - queue->size;
    }

    return queue->values[pos];
}

int get_queue_size(queue_t *queue)
{
    /**
     * @brief returns number of items currently in queue
     *
     */
    return queue->num_of_items;
}

queue_t *reallocate_queue(queue_t *queue, int new_size)
{
    //  allocate new array of ptrs
    void **new_values = mallocate(POINTER_SIZE * new_size);
    int i = 0;

    // copy values
    while (i < queue->num_of_items)
    {
        if (queue->size == queue->first_pos)
        {
            queue->first_pos = 0;
        }

        new_values[i] = queue->values[queue->first_pos];

        i++;
        queue->first_pos++;
    }

    // init other values to NULL
    for (int j = i; j < new_size; j++)
    {
        new_values[j] = NULL;
    }

    free(queue->values);

    // update queue variables
    queue->values = new_values;
    queue->first_pos = 0;
    queue->end_pos = i;
    queue->size = new_size;

    if (queue->size == queue->end_pos)
    {
        queue->end_pos = 0;
    }

    return queue;
}
