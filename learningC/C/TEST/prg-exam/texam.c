/*
 * File name: texam.c
 * Date:      2017/01/16 17:00
 * Author:    Jan Faigl
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "exam_utils.h"
#include "save_jpeg.h"
#include "xwin_sdl.h"

#define WIDTH 640
#define HEIGHT 480

#define OPT_ARGUMENT "--anim"
#define COPY_COMM "cp"
#define SWAP_COMM "sw"
#define JPEG_SUFFIX ".jpg\0"
#define PPM_SUFFIX ".ppm\0"

#define ERROR_OK 0

#define ERROR_WRONG_ARGS 100
#define ERR_MESS_WRONG_ARGS "Wrong arguments provided!\n"

#define ERROR_WRONG_NAME 101
#define ERR_MESS_WRONG_NAME "File not found!\n"

#define ERROR_BAD_COMMAND 102
#define ERR_MESS_BAD_COMMAND "Bad command format!\n"

#define ERROR_BAD_RANGE 103
#define ERR_MESS_BAD_RANGE "Command indexes out of range!\n"

typedef struct
{
   int width;
   int height;
   unsigned char *im;
} Image;

int control(Image image, char *list_of_oper_file, char *output_im_name, bool animation);
int read_args(char **input_file, char **list_of_oper_file, char **output_im_name, bool *animation, int argc, char *argv[]);
int change_image(unsigned char *image, int width, int heigh, FILE *inst_file, bool animation);
bool copy(unsigned char *image, int im_width, int im_heigh, int start_w, int start_h, int len_w, int len_h, int place_w, int place_h);
bool swap(unsigned char *image, int im_width, int im_heigh, int start_w, int start_h, int len_w, int len_h, int place_w, int place_h);

int main(int argc, char *argv[])
{
   int error = ERROR_OK;

   // read arguments
   char *input_file;
   char *list_of_oper_file;
   char *output_im_name;
   bool animation;
   error = read_args(&input_file, &list_of_oper_file, &output_im_name, &animation, argc, argv);
   if (error != ERROR_OK)
   {
      fprintf(stderr, ERR_MESS_WRONG_ARGS);
      return ERROR_WRONG_ARGS;
   }

   // read image
   Image image;
   image.im = xwin_load_image(input_file, &image.width, &image.height);
   if (image.im == NULL)
   {
      fprintf(stderr, ERR_MESS_WRONG_NAME);
      return ERROR_WRONG_NAME;
   }

   // catch errors
   error = control(image, list_of_oper_file, output_im_name, animation);
   switch (error)
   {
   case ERROR_WRONG_NAME:
      fprintf(stderr, ERR_MESS_WRONG_NAME);
      break;
   case ERROR_BAD_COMMAND:
      fprintf(stderr, ERR_MESS_BAD_COMMAND);
      break;
   case ERROR_BAD_RANGE:
      printf(ERR_MESS_BAD_RANGE);
      break;
   default:
      break;
   }

   free(image.im);

   return error;
}

int control(Image image, char *list_of_oper_file, char *output_im_name, bool animation)
{
   int error = ERROR_OK;
   printf("anim: %d\n", animation);

   // init window
   if (animation)
   {
      xwin_init(image.width, image.height);
      xwin_redraw(image.width, image.height, image.im);
      delay(2000);
   }

   // open instructions
   FILE *inst_file = fopen(list_of_oper_file, "r");
   if (inst_file == NULL)
   {
      return ERROR_WRONG_NAME;
   }

   // compute
   if ((error = change_image(image.im, image.width, image.height, inst_file, animation)) != ERROR_OK)
   {
      return error;
   }

   fclose(inst_file);

   // save image
   if (strstr(output_im_name, JPEG_SUFFIX))
   {
      save_image_jpeg(image.width, image.height, image.im, output_im_name);
   }
   else if (strstr(output_im_name, PPM_SUFFIX))
   {
      save_image_rgb(image.width, image.height, image.im, output_im_name);
   }

   // end window
   if (animation)
   {
      xwin_close();
   }
   return error;
}

int read_args(char **input_file, char **list_of_oper_file, char **output_im_name, bool *animation, int argc, char *argv[])
{
   printf("%d\n", argc);
   if (argc >= 4)
   {
      *input_file = argv[1];
      *list_of_oper_file = argv[2];
      *output_im_name = argv[3];
      *animation = false;
      // opt
      if (argc == 5)
      {
         if (strcmp(argv[4], OPT_ARGUMENT) == 0)
         {
            *animation = true;
         }
      }
      return ERROR_OK;
   }
   else
   {
      return ERROR_WRONG_ARGS;
   }
}

int change_image(unsigned char *image, int width, int height, FILE *inst_file, bool animation)
{
   while (!feof(inst_file))
   {
      char cmd[3] = {[2] = '\0'};
      int start_w;
      int start_h;
      int len_w;
      int len_h;
      int place_w;
      int place_h;

      if (fscanf(inst_file, "%2s %d %d %d %d %d %d", cmd, &start_w, &start_h, &len_w, &len_h, &place_w, &place_h) != 7)
      {
         return ERROR_BAD_COMMAND;
      }

      if (strcmp(cmd, COPY_COMM) == 0)
      {
         printf("cp %d\n", place_h);
         if (copy(image, width, height, start_w, start_h, len_w, len_h, place_w, place_h))
         {
            return ERROR_BAD_RANGE;
         }
      }
      else if (strcmp(cmd, SWAP_COMM) == 0)
      {
         printf("sw %d\n", place_h);
         if (swap(image, width, height, start_w, start_h, len_w, len_h, place_w, place_h))
         {
            return ERROR_BAD_RANGE;
         }
      }
      else
      {
         return ERROR_BAD_COMMAND;
      }

      if (animation)
      {
         xwin_redraw(width, height, image);
         delay(1000);
      }
   }
   return ERROR_OK;
}

bool copy(unsigned char *image, int im_width, int im_heigh, int start_w, int start_h, int len_w, int len_h, int place_w, int place_h)
{
   for (int dh = 0; dh < len_h; dh++)
   {
      for (int dw = 0; dw < len_w; dw++)
      {
         int start_pos_h = dh + start_h;
         int start_pos_w = dw + start_w;
         int place_pos_h = dh + place_h;
         int place_pos_w = dw + place_w;

         if (
             start_pos_h < im_heigh && start_pos_w < im_width &&
             start_pos_h >= 0 && start_pos_w >= 0 &&
             place_pos_h < im_heigh && place_pos_w < im_width &&
             place_pos_h >= 0 && place_pos_w >= 0)
         {

            // note that is it RGB!
            for (int i = 0; i < 3; i++)
            {
               image[((place_pos_h * im_width) + place_pos_w) * 3 + i] = image[((start_pos_h * im_width) + start_pos_w) * 3 + i];
            }
         }
         else
         {
            return true;
         }
      }
   }
   return false;
}

bool swap(unsigned char *image, int im_width, int im_heigh, int start_w, int start_h, int len_w, int len_h, int place_w, int place_h)
{
   for (int dh = 0; dh < len_h; dh++)
   {
      for (int dw = 0; dw < len_w; dw++)
      {
         int start_pos_h = dh + start_h;
         int start_pos_w = dw + start_w;
         int place_pos_h = dh + place_h;
         int place_pos_w = dw + place_w;

         if (
             start_pos_h < im_heigh && start_pos_w < im_width &&
             start_pos_h >= 0 && start_pos_w >= 0 &&
             place_pos_h < im_heigh && place_pos_w < im_width &&
             place_pos_h >= 0 && place_pos_w >= 0)
         {

            // note that is it RGB!
            for (int i = 0; i < 3; i++)
            {
               unsigned char t = image[((place_pos_h * im_width) + place_pos_w) * 3 + i];
               image[((place_pos_h * im_width) + place_pos_w) * 3 + i] = image[((start_pos_h * im_width) + start_pos_w) * 3 + i];
               image[((start_pos_h * im_width) + start_pos_w) * 3 + i] = t;
            }
         }
         else
         {
            // bad range
            return true;
         }
      }
   }
   return false;
}
