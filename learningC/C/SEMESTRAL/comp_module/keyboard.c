#include "keyboard.h"

#include "../libs/prg_io_nonblock.h"
#include "../libs/messages.h"
#include "../libs/event_queue.h"
#include "../libs/utils.h"
#include "../libs/prg_io_nonblock.h"
/**
 * @brief reads input from user ->
 * decides the nature of event ->
 * pushes event to queue *
 * @param data pointer to integer containing pipe
 * @return void* NULL
 */
void *keyboard_thread(void *data)
{
    log_mes(NOTE, __func__, __LINE__, __FILE__, START);

    call_termios(0);
    unsigned char c = '0';
    event ev;

    // decide action
    while (!is_quit())
    {
        // read one char from stdin until timeout, then proceed
        int check = stdin_getc_timeout(STDIN_FD, READ_TIMEOUT_MS, &c);
        switch (check)
        {
        case 1:
            // char read
            ev.type = decide_event(c);
            if (ev.type != EV_TYPE_NUM)
            {
                queue_push(ev);
            }
            break;
        case 0:
            // timeout
            break;
        }
    }
    call_termios(1);
    log_mes(NOTE, __func__, __LINE__, __FILE__, END);
    return NULL;
}

int decide_event(const char c)
{
    switch (c)
    {
    case 'q':
        // terminates just this application
        return EV_QUIT;

    case 'a':
        // abort - pauses computation
        return EV_ABORT;

    default:
        return EV_TYPE_NUM;
    }
}