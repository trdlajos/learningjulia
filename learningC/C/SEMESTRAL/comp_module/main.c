#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include "../libs/utils.h"
#include "../libs/prg_io_nonblock.h"
#include "comp_in.h"
#include "comp_out.h"
#include "keyboard.h"

#define THRD_SUCCES 0
#define PIPE_OUT_DEFAULT_NAME "/tmp/computational_module.out"
#define PIPE_IN_DEFAULT_NAME "/tmp/computational_module.in"

int main(int argc, char const *argv[])
{
    int ret = EXIT_SUCCESS;

    // init pipe locations
    const char *pipe_in_loc = argc > 1 ? argv[1] : PIPE_IN_DEFAULT_NAME;
    const char *pipe_out_loc = argc > 2 ? argv[2] : PIPE_OUT_DEFAULT_NAME;

    int pipe_in = io_open_read(pipe_in_loc);
    int pipe_out = io_open_write(pipe_out_loc);

    my_assert(pipe_in != -1 && pipe_out != -1, __func__, __LINE__, __FILE__);

    enum
    {
        IN_THRD,
        OUT_THRD,
        KEYBOARD_THRD,
        NUM_THRDS,
    };

    void *(*thrd_functions[])(void *) = {in_thread, out_thread, keyboard_thread};
    const char *threads_names[] = {"IN", "OUT", "KEYBOARD"};
    pthread_t threads[NUM_THRDS];

    void *thrd_data[NUM_THRDS] = {};
    thrd_data[IN_THRD] = &pipe_in;
    thrd_data[OUT_THRD] = &pipe_out;

    queue_init();

    for (int i = 0; i < NUM_THRDS; ++i)
    {
        int r = pthread_create(&threads[i], NULL, thrd_functions[i], thrd_data[i]);
        printf("Create thread '%s' %s\r\n", threads_names[i], (r == THRD_SUCCES ? "OK" : "FAIL"));
    }


    int ex;
    for (int i = 0; i < NUM_THRDS; ++i)
    {
        printf("Call join to the thread %s\r\n", threads_names[i]);
        int r = pthread_join(threads[i], (void *)&ex);
        printf("Joining the thread %s has been %s - exit value %i\r\n", threads_names[i], (r == THRD_SUCCES ? "OK" : "FAIL"), ex);
    }

    queue_cleanup();

    io_close(pipe_in);
    io_close(pipe_out);

    return ret;
}
