
#include "../libs/event_queue.h"
#include "../libs/messages.h"


// version definition
#define VER_MAJOR 1
#define VER_MINOR 0
#define VER_PATCH 1

// startup definition

#define STARTUP_MESS                                 \
    {                                                \
        'T', 'R', 'D', 'L', 'A', 'J', 'O', 'S', '\0' \
    }

void *out_thread(void *data);

void process_pipe_message(event *const ev, message *response);

void set_version(message *response);

message set_startup();
