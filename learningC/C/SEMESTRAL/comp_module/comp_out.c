#include "comp_out.h"
#include <stdlib.h>
#include <stdio.h>
#include "../libs/utils.h"
#include "../libs/prg_io_nonblock.h"
#include "module_compute.h"
#include <stdint.h>
#include <unistd.h>

/**
 * @brief According to message recieved performs desired action
 *
 * @param ev - event recieved
 * @param response - message that will be sent as a response to control module
 */
void process_pipe_message(event *const ev, message *response)
{
    // debug
    my_assert(ev != NULL && ev->type == EV_PIPE_IN_MESSAGE && ev->data.msg, __func__, __LINE__, __FILE__);
    // log_mes(NOTE, __func__, __LINE__, __FILE__, "started proccesing message");

    ev->type = EV_TYPE_NUM;
    const message *incom_msg = ev->data.msg;

    switch (incom_msg->type)
    {
    case MSG_GET_VERSION:
        set_version(response);
        break;
    case MSG_TERMINATE:
        event quit = {.type = EV_QUIT};
        queue_push(quit);
        break;
    case MSG_SET_COMPUTE:
        set_module_comp(incom_msg, response);
        break;
    case MSG_COMPUTE:
        init_module_comp(incom_msg, response);
        // if init succesful, push computation event
        if (response->type == MSG_OK)
        {
            event comp_data = {.type = EV_COMPUTE_DATA};
            queue_push(comp_data);
        }
        break;
    default:
        log_mes(ERROR, __func__, __LINE__, __FILE__, "unknown message type");
        break;
    }
    free(ev->data.msg);
    ev->data.msg = NULL;
}

/**
 * @brief pops event from queue ->
 * attach proper msg to event according to ev type or perform action according to ev.type ->
 * write to pipe(if neccesary)
 *
 * Based on tutorial from prof. Faigl
 *
 * @param data pointer to integer containing pipe
 * @return void*
 */
void *out_thread(void *data)
{

    // debug
    log_mes(NOTE, __func__, __LINE__, __FILE__, START);
    my_assert(data != NULL, __func__, __LINE__, __FILE__);

    int pipe_out = *(int *)data;
    uint8_t msg_buff[sizeof(message)];
    int msg_len;
    bool quit = false;

    // send startup message
    event startup = {.type = EV_START};
    queue_push(startup);

    do
    {
        event ev = queue_pop();
        message msg_response;
        msg_response.type = MSG_NBR;

        switch (ev.type)
        {
        case EV_START:
            msg_response = set_startup();
            break;
        case EV_QUIT:
            log_mes(NOTE, __func__, __LINE__, __FILE__, "quit sent");
            if (!is_module_computing())
            {
                std_mes(NOTE, "Quitting!");
                set_quit();
            }
            else
            {
                std_mes(WARNING, "Finish computation fisrt!");
            }
            break;
        case EV_COMPUTE_DATA:
            compute_pixel(&msg_response);
            // if computation not done, proceed
            if (msg_response.type != MSG_DONE)
            {
                event comp_data = {.type = EV_COMPUTE_DATA};
                queue_push(comp_data);
            }
            break;
        case EV_PIPE_IN_MESSAGE:
            process_pipe_message(&ev, &msg_response);
            break;
        case EV_ABORT:
            msg_response.type = MSG_ABORT;
            log_mes(NOTE, __func__, __LINE__, __FILE__, "abort request sent");
            break;
        default:
            break;
        }

        // send to pipe
        if (msg_response.type != MSG_NBR)
        {
            my_assert(fill_message_buf(&msg_response, msg_buff, sizeof(message), &msg_len), __func__, __LINE__, __FILE__);
            if (write(pipe_out, msg_buff, msg_len) == msg_len)
            {
                // debug
                //  log_mes(NOTE, __func__, __LINE__, __FILE__, "data send to pipe\n");
                //  printf("type of send MSG: %d\n", msg_buff[0]);
            }
            else
            {
                log_mes(ERROR, __func__, __LINE__, __FILE__, "data could not be send to pipe\n");
            }
        }

        quit = is_quit();
    } while (!quit);

    log_mes(NOTE, __func__, __LINE__, __FILE__, END);
    return NULL;
}

/**
 * @brief Set the version msg
 *
 * @param response
 */
void set_version(message *response)
{
    response->type = MSG_VERSION;
    response->data.version.major = VER_MAJOR;
    response->data.version.minor = VER_MINOR;
    response->data.version.patch = VER_PATCH;
}

/**
 * @brief Set the startup msg
 *
 * @return response
 */
message set_startup()
{
    message msg = {msg.type = MSG_STARTUP, .data.startup.message = STARTUP_MESS};
    return msg;
}
