#include "module_compute.h"
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include "../libs/utils.h"

struct
{

    // complex number constant
    double c_re;
    double c_im;

    // num of iterations
    uint8_t max_iter;

    // cursor (complex)
    double cur_re;
    double cur_im;

    // cursor (pixels), relative to the chunk!
    uint8_t x_pos_re;
    uint8_t y_pos_im;

    // step
    double step_re;
    double step_im;

    // overall length and width of chunk in pixels
    uint8_t max_x;
    uint8_t max_y;

    // starting real number
    double start_re;

    // id of chunk currently computed
    uint8_t cid;

    // control variables
    bool computing;
    bool been_set;

} comp_memory = {
    .computing = false,
    .been_set = false,
};

/**
 * @brief Set the compute parameters according to incom message data
 *
 */
void set_module_comp(const message *incom_msg, message *response)
{
    if (!comp_memory.computing)
    {
        comp_memory.c_im = incom_msg->data.set_compute.c_im;
        comp_memory.c_re = incom_msg->data.set_compute.c_re;
        comp_memory.step_im = incom_msg->data.set_compute.d_im;
        comp_memory.step_re = incom_msg->data.set_compute.d_re;
        comp_memory.max_iter = incom_msg->data.set_compute.n;
        response->type = MSG_SET_OK;
        comp_memory.been_set = true;
        log_mes(NOTE, __func__, __LINE__, __FILE__, "parameters of computation passed");
    }
    else
    {
        response->type = MSG_SET_ERROR;
        log_mes(NOTE, __func__, __LINE__, __FILE__, "parameters of computation not passed!");
    }
}

/**
 * @brief Inits the computation of pixels of specified chunk according to insom msg data
 *
 */
void init_module_comp(const message *incom_msg, message *response)
{
    // log_mes(NOTE, __func__, __LINE__, __FILE__, "trying to init computation");

    if (comp_memory.computing == false && comp_memory.been_set)
    {
        comp_memory.cid = incom_msg->data.compute.cid;
        comp_memory.cur_im = incom_msg->data.compute.im;
        comp_memory.start_re = comp_memory.cur_re = incom_msg->data.compute.re;

        // update max variables
        comp_memory.max_x = incom_msg->data.compute.n_re;
        comp_memory.max_y = incom_msg->data.compute.n_im;

        // update cursor
        comp_memory.x_pos_re = 0;
        comp_memory.y_pos_im = 0;

        comp_memory.computing = true;
        response->type = MSG_OK;

        // log_mes(NOTE, __func__, __LINE__, __FILE__, "computation started!");
    }
    else if (!comp_memory.been_set)
    {
        // computation has not required parameters
        response->type = MSG_NOT_SET;
        log_mes(WARNING, __func__, __LINE__, __FILE__, "computation not set!");
    }
    else
    {
        response->type = MSG_ERROR;

        log_mes(WARNING, __func__, __LINE__, __FILE__, "computation not started!");
    }
}

/**
 * @brief computes the iteration at which point in complex plain starts to diverge
 *
 */
void compute_pixel(message *msg)
{
    if (can_compute())
    {
        int curr_iteration = 0;

        double z_re = comp_memory.cur_re;
        double z_im = comp_memory.cur_im;

        // computation itself
        while (abs_complex(z_re, z_im) < 2)
        {
            double new_z_re;
            double new_z_im;

            square(z_re, z_im, &new_z_re, &new_z_im);

            z_re = new_z_re + comp_memory.c_re;
            z_im = new_z_im + comp_memory.c_im;

            curr_iteration += 1;

            if (curr_iteration == comp_memory.max_iter)
            {
                break;
            }
        }

        // update msg
        msg->type = MSG_COMPUTE_DATA;
        msg->data.compute_data.cid = comp_memory.cid;
        msg->data.compute_data.i_re = comp_memory.x_pos_re;
        msg->data.compute_data.i_im = comp_memory.y_pos_im;
        msg->data.compute_data.iter = curr_iteration;

        // move on x axis to next point in complex plain
        comp_memory.cur_re += comp_memory.step_re;
        comp_memory.x_pos_re++;

        // log_mes(NOTE, __func__, __LINE__, __FILE__, "iteration computed!");
    }
    else
    {
        // log_mes(NOTE, __func__, __LINE__, __FILE__, "end of computation");
        msg->type = MSG_DONE;

        // computation finished
        comp_memory.computing = false;
    }
}

/**
 * @brief computes square of given complex num
 *
 */
void square(double re, double im, double *new_re, double *new_im)
{
    *new_im = 2 * re * im;
    *new_re = (re * re) - (im * im);
}

/**
 * @brief computes the absolute number of complex num
 *
 */
double abs_complex(double re, double im)
{
    return sqrt((re * re) + (im * im));
}

/**
 * @brief Checks if we can continue in computation or if computation finished,
 * also makes sure we compute the right numbers
 *
 */
bool can_compute(void)
{

    // if max x reach, come to start on x axis and move down on y axis
    if (comp_memory.x_pos_re >= comp_memory.max_x)
    {
        comp_memory.cur_re = comp_memory.start_re;
        comp_memory.cur_im = comp_memory.cur_im + comp_memory.step_im;
        comp_memory.y_pos_im++;
        comp_memory.x_pos_re = 0;
    }

    // if max y reached, computation finished
    if (comp_memory.y_pos_im >= comp_memory.max_y)
    {
        comp_memory.computing = false;
        return false;
    }

    return true;
}

bool is_module_computing()
{
    return comp_memory.computing;
}