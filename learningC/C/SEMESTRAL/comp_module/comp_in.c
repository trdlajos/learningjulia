
#include "comp_in.h"
#include <stdlib.h>
#include "../libs/event_queue.h"
#include "../libs/utils.h"
#include "../libs/prg_io_nonblock.h"
#include "../libs/messages.h"
#include <stdint.h>


/**
 * @brief reads message sent through pipe, pushes event into queue for proccesing
 *
 * Based on tutorial from prof. Faigl
 *
 * @param data - pointer to pipe
 * @return void* - NULL
 */
void *in_thread(void *data)
{
    log_mes(NOTE, __func__, __LINE__, __FILE__, START);
    my_assert(data != NULL, __func__, __LINE__, __FILE__);


    int pipe_in = *(int *)data;
    bool end = false;
    uint8_t msg_buff[sizeof(message)];
    int i = 0;
    int mess_len = 0;

    unsigned char c;

    // discard garbage from pipe
    while (io_getc_timeout(pipe_in, READ_TIMEOUT_MS, &c) > 0)
    {
    }

    while (!end)
    {

        // read from pipe
        int r = io_getc_timeout(pipe_in, READ_TIMEOUT_MS, &c);
        if (r > 0)
        {
            // byte read

            if (i == 0)
            {
                // first byte has been read - decide nature of message
                if (get_message_size(c, &mess_len))
                {
                    msg_buff[i++] = c;
                }
                else
                {
                    log_mes(ERROR, __func__, __LINE__, __FILE__, "Unknown message type");
                }
            }
            else
            {
                // read rest of the message
                msg_buff[i++] = c;
            }

            if (mess_len > 0 && i == mess_len)
            {
                // all bytes of message have been read
                message *msg = (message *)allocate(sizeof(message));

                // log_mes(NOTE, __func__, __LINE__, __FILE__, "message has been read");

                if (parse_message_buf(msg_buff, mess_len, msg))
                {

                    // log_mes(NOTE, __func__, __LINE__, __FILE__, "message has been parsed");
                    event ev = {.type = EV_PIPE_IN_MESSAGE};
                    ev.data.msg = msg;

                    queue_push(ev);
                }
                else
                {
                    // could not parse message
                    log_mes(ERROR, __func__, __LINE__, __FILE__, "cannot parse message type");
                    free(msg);
                }
                i = mess_len = 0;
            }
        }
        else if (r == 0)
        {
            // timeout
        }
        else
        {
            // terminating program
            log_mes(ERROR, __func__, __LINE__, __FILE__, IO_ERROR_READING);
            set_quit();
            event ev = {.type = EV_QUIT};
            queue_push(ev);
        }
        end = is_quit();
    }

    log_mes(NOTE, __func__, __LINE__, __FILE__, END);
    return NULL;
}