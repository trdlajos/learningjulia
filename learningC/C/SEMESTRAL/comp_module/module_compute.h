
#include <stdbool.h>
#include "../libs/messages.h"

// set up comp
void set_module_comp(const message *msg, message *response);
void init_module_comp(const message *msg, message *response);

// computation
void compute_pixel(message *msg);
double abs_complex(double re, double im);
void square(double re, double im, double *new_re, double *new_im);

// getters, setters
bool can_compute(void);
bool is_module_computing();
