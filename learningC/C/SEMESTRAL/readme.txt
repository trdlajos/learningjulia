Semestral project 
author: Josef Trdla 

User can use both terminal or window to pass commands to control application(I tried to prevent user for being able to resize window, 
but this appears to not be working in windows WSL)

User is able to change parameters of computation like boundaries of complex plane, 
constant C or maximum number of iteration allowed, but only through terminal

Also there are three preset resolutions, that user can cycle through using - or +
Image displayed can be exported as a .png file

Description of commands(control application):

EXIT:
q - ends application 
t - ends both control application and computation module

COMPUTATION:
s - (re)set computation according to parameters stored inside control application, also completly resets the computation

u - updates parameters of computation according to user input - c and maximal number of iteration 
i - updates points defining complex pane we are computing
(note that after these two commands, module has to be reset by set comutation command)

c - (re)start computation(after computation ended succesfully, buffer needs to be cleared, or reset)
a - abort computation(pauses it, lets the module finish computation of last chunk started first)
l - clears data in buffer(grid), refreshes window

RESOLUTION
+ / m - changes resolution to higher one
- / n - changes resolution to lower one 

OTHER:
g - asks for version of computation module
e - exports current image displayed as a png file with name Computed_plane.png to a file in with main_app file is located

Description of terminal commands(computation module):
q - ends computation module (note that if computation module has been restared, 
    set up comutation command need to be send first in order to restart computation)
a - aborts computation(pause)
