/**
 * Contains various logging functions, dynamic aloc function, and change terminal func
 */


#ifndef __UTILS_H__
#define __UTILS_H__


#include <stdbool.h>
#include <stdlib.h>

// defines logging types
#define NOTE "NOTE"
#define WARNING "WARNING"
#define ERROR "ERROR"
#define REQUEST "Please, enter "

// defines some fruequently used messages
#define START "START"
#define END "END"
#define MEM_ERROR_MES "Memory error"
#define BAD_INPUT "Your input has not been recognized!"
#define TOO_MUCH_BAD_INPUTS "There were too much bad inputs, try again!"
#define ERROR_COMP_RUNNING "You have to pause computation first!"

#define MEM_ERROR_CODE 101
#define ASSERT_ERROR_MES "ERROR: my_assert FAIL: (func_name on line in name_of_file)"
#define ASSERT_ERROR_CODE 105

void *allocate(size_t size);

void my_assert(bool r, const char *fcname, int line, const char *fname);

void call_termios(int reset);

void log_mes(const char *type, const char *funcname, int line, const char *filename, const char *status);

void std_mes(const char *type, const char *msg);

#endif