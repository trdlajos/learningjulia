/*
 * Filename: xwin_sdl.h
 * Date:     2015/06/18 14:37
 * Author:   Jan Faigl
 */

#ifndef __XWIN_H__
#define __XWIN_H__

int xwin_init(int w, int h);
void xwin_close();
void xwin_redraw(int w, int h, unsigned char *img);
void export_im(char *fil_nam);
void xwin_poll_events(void);
void update_res(int w, int h);
#endif