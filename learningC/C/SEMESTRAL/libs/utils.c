#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

void *allocate(size_t size)
{
    void *ret = malloc(size);
    if (ret == NULL)
    {
        fprintf(stderr, MEM_ERROR_MES);
        exit(MEM_ERROR_CODE);
    }
    return ret;
}

void my_assert(bool r, const char *fcname, int line, const char *fname)
{
    if (!r)
    {
        fprintf(stderr, ASSERT_ERROR_MES);
        fprintf(stderr, "%s %d %s\n", fcname, line, fname);
        exit(ASSERT_ERROR_CODE);
    }
}

void call_termios(int reset)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset)
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
    else
    {
        tioOld = tio; // backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

/**
 * @brief Logging function
 *
 * @param type type of message - INFORM/WARNING/ERROR
 * @param funcname name of func/thred logged
 * @param line line at witch logger was activated
 * @param filename filename of logger origin
 * @param status description
 */
void log_mes(const char *type, const char *funcname, int line, const char *filename, const char *status)
{
    fprintf(stderr, "%s: %s: line %d in %s: %s\n", type, funcname, line, filename, status);
}

/**
 * @brief Terminal user interface message, for communication with user
 *
 * @param type Determines general type of message
 * @param msg Message to user
 */
void std_mes(const char *type, const char *msg)
{
    printf("%s: %s\n", type, msg);
}
