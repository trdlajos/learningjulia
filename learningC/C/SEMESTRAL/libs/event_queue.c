#include "event_queue.h"
#include <pthread.h>

#include <stdlib.h>

// example of module based global var

typedef struct
{
    event queue[BASE_CAPACITY]; // we assume 32 will be enough
    int first;
    int last;
    bool quit;

    pthread_mutex_t mtx;
    pthread_cond_t cnd;

} queue;

static queue q = {.first = 0, .last = 0};

void queue_init(void)
{
    /**
     * @brief inits queue thread parameters
     *
     */
    pthread_mutex_init(&(q.mtx), NULL);
    pthread_cond_init(&(q.cnd), NULL);
}

void queue_cleanup(void)
{
    while (q.first != q.last)
    {
        event ev = queue_pop();
        if (ev.data.msg != NULL)
        {
            free(ev.data.msg);
        }
    }
}

event queue_pop(void)
{
    event ev = {.type = EV_TYPE_NUM};
    pthread_mutex_lock(&(q.mtx));
    while (!q.quit && q.first == q.last)
    {
        pthread_cond_wait(&(q.cnd), &(q.mtx));
    }
    if (q.first != q.last)
    {
        ev = q.queue[q.last];
        q.last = (q.last + 1) % BASE_CAPACITY;
        pthread_cond_broadcast(&(q.cnd));
    }

    pthread_mutex_unlock(&(q.mtx));

    return ev;
}

void queue_push(event ev)
{
    pthread_mutex_lock(&(q.mtx));
    while (((q.first + 1) % BASE_CAPACITY) == q.last)
    {
        pthread_cond_wait(&(q.cnd), &(q.mtx));
    }

    q.queue[q.first] = ev;
    q.first = (q.first + 1) % BASE_CAPACITY;
    pthread_cond_broadcast(&(q.cnd));
    pthread_mutex_unlock(&(q.mtx));
}

bool is_quit()
{
    bool quit;

    pthread_mutex_lock(&(q.mtx));
    quit = q.quit;
    pthread_mutex_unlock(&(q.mtx));
    return quit;
}
void set_quit()
{
    pthread_mutex_lock(&(q.mtx));
    q.quit = true;
    pthread_mutex_unlock(&(q.mtx));
}