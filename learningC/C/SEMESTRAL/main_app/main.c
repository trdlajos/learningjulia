
#include "main.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "../libs/utils.h"
#include "computation.h"
#include "gui.h"

/**
 * @brief pops event from queue ->
 * attach proper msg to event according to ev type or perform action according to ev.type ->
 * write to pipe(if neccesary)
 *
 */

void *main_thread(void *data)
{
    log_mes(NOTE, __func__, __LINE__, __FILE__, START);
    my_assert(data != NULL, __func__, __LINE__, __FILE__);

    int pipe_out = *(int *)data;
    bool quit = false;

    // mesage that will be send to computation module
    message output_msg;

    // init computation, visulation
    computation_init();
    gui_init();
    clear_buffer();

    do
    {
        event ev = queue_pop();
        output_msg.type = MSG_NBR;

        switch (ev.type)
        {
        case EV_QUIT:
            std_mes(NOTE, "Quiting control module...");
            set_quit();
            break;
        case EV_QUIT_BOTH:
            std_mes(NOTE, "Quiting both control module and computation module...");
            set_quit();
            output_msg.type = MSG_TERMINATE;
            break;
        case EV_GET_VERSION:
            output_msg.type = MSG_GET_VERSION;
            break;
        case EV_ABORT:
            if (!is_aborted() && is_computing())
            {
                request_abort();
                std_mes(NOTE, "Aborting...");
            }
            else
            {
                std_mes(WARNING, "Nothing to abort");
            }
            break;
        case EV_SET_COMPUTE:
            if (is_aborted() || !is_computing())
            {
                set_compute(&output_msg);
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_START_COMPUTE:

            if (!is_computing() || is_aborted())
            {
                enable_computation();
                std_mes(NOTE, "Computation order recieved!");
            }
            else
            {
                std_mes(WARNING, "One computation currenty underway, cannot start another one!");
            }
        case EV_COMPUTE:
            // log_mes(NOTE, __func__, __LINE__, __FILE__, "compute order recieved");
            if (is_set() && !is_done())
            {
                compute(&output_msg);
            }
            else
            {
                std_mes(WARNING, "Please, set up new computation fist!");
            }
            break;
        case EV_PIPE_IN_MESSAGE:
            process_pipe_message(&ev);
            break;
        case EV_CLEAR_BUFFER:
            if (is_aborted() || !is_computing())
            {
                clear_buffer();
                std_mes(NOTE, "Buffer cleared");
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_UPDATE_PLANE:
            if (is_aborted() || !is_computing())
            {
                update_complex_plane(ev.data.msg);
                std_mes(NOTE, "Plane was updated!");
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_UPDATE_PARAMETERS:
            if (is_aborted() || !is_computing())
            {
                update_parameters(ev.data.msg);
                std_mes(NOTE, "Parameters were updated!");
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_SHRINK_RESOLUTIN:
            if (is_aborted() || !is_computing())
            {
                change_res(SHRINK);
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_ENLARGE_RESOLUTION:
            if (is_aborted() || !is_computing())
            {
                change_res(ENLARGE);
            }
            else
            {
                std_mes(WARNING, ERROR_COMP_RUNNING);
            }
            break;
        case EV_EXPORT:
            save_im();
            std_mes(NOTE, "Picture exported!");
            break;
        default:
            break;
        }

        // send message to comp module
        send_message(output_msg, pipe_out);

        quit = is_quit();
    } while (!quit);

    // cleanup computation, visulation
    computation_cleanup();
    gui_cleanup();

    log_mes(NOTE, __func__, __LINE__, __FILE__, END);
    return NULL;
}

/**
 * @brief Performs action according to message given
 *
 */
void process_pipe_message(event *const ev)
{
    // log_mes(NOTE, __func__, __LINE__, __FILE__, "started proccesing message");
    my_assert(ev != NULL && ev->type == EV_PIPE_IN_MESSAGE && ev->data.msg, __func__, __LINE__, __FILE__);

    ev->type = EV_TYPE_NUM;
    const message *msg = ev->data.msg;

    switch (msg->type)
    {
    case MSG_OK:
        // log_mes(NOTE, __func__, __LINE__, __FILE__, "comp_module OK");
        break;
    case MSG_VERSION:
        printf("INFO: Module version %d.%d-p%d\n", msg->data.version.major, msg->data.version.minor, msg->data.version.patch);
        break;
    case MSG_STARTUP:
        printf("Module started:\n");
        printf("%s\n", msg->data.startup.message);
        break;
    case MSG_COMPUTE_DATA:
        update_data(&(msg->data.compute_data));
        break;
    case MSG_DONE:
        gui_refresh();
        if (is_done())
        {
            std_mes(NOTE, "Computation finished!");
        }
        else if (is_abort_requested())
        {
            abort_computation();
            std_mes(NOTE, "Aborted!");
        }
        else
        {
            // if abort not requested, computed next chunk
            event ev = {.type = EV_COMPUTE};
            queue_push(ev);
        }
        break;
    case MSG_ABORT:
        event ev = {.type = EV_ABORT};
        queue_push(ev);
        break;
    case MSG_NOT_SET:
        printf("%d\n", msg->type);
        std_mes(WARNING, "Computation module has no parameters for computation, set the computation first!");
        break;
    case MSG_SET_ERROR:
        std_mes(WARNING, "Computation cannot be set while the module is computing!");
        break;
    case MSG_SET_OK:
        std_mes(NOTE, "Passing parameters of new computation has been succesful!");
        break;
    default:
        log_mes(ERROR, __func__, __LINE__, __FILE__, "unknown message type");
        break;
    }
    free(ev->data.msg);
    ev->data.msg = NULL;
}

void send_message(message output_msg, int pipe_out)
{
    // send to pipe
    if (output_msg.type != MSG_NBR)
    {
        uint8_t msg_buff[sizeof(message)];
        int msg_len;

        my_assert(fill_message_buf(&output_msg, msg_buff, sizeof(message), &msg_len), __func__, __LINE__, __FILE__);
        if (write(pipe_out, msg_buff, msg_len) == msg_len)
        {

            // log_mes(NOTE, __func__, __LINE__, __FILE__, "data send to pipe\n");
        }
        else
        {
            log_mes(ERROR, __func__, __LINE__, __FILE__, "data could not be send to pipe");
        }
    }
}
