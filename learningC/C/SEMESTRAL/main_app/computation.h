/*Implementation of computation
 *all functions connected to computation are here
 */

#ifndef __COMPUTATION_H__
#define __COMPUTATION_H__

#include <stdbool.h>
#include <stdlib.h>
#include "../libs/messages.h"

void computation_init(void);
void computation_cleanup(void);

// setting, restarting, reseting
void set_compute(message *msg);
void clear_buffer();
bool change_res(int how);
void reset_computation(void);

// computation
bool compute(message *msg);
void update_data(const msg_compute_data *compute_data);

// image handling
void get_grid_size(int *h, int *w);
void update_image(int w, int h, unsigned char *img);

// updating parameters
void update_complex_plane(message *msg);
void update_parameters(message *msg);
bool change_res(int how);

// setters and getters
void abort_computation(void);
void enable_computation(void);
bool is_aborted(void);
void request_abort(void);
bool is_abort_requested(void);
bool is_set(void);
bool is_computing(void);
bool is_done(void);

#endif