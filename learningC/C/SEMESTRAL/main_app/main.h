/*Implementation of main_thread(thread responsible for output and proccesing of messages)
 */
#include "../libs/messages.h"
#include "../libs/event_queue.h"

void process_pipe_message(event *const ev);
void *main_thread(void *data);
void send_message(message output_msg, int pipe_out);
