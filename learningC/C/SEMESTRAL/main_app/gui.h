/*Implementation of gui(window handling)
 * Based on tutorial from prof. Faigl, with many changes
 *
 */


#ifndef __GUI_H__
#define __GUI_H__

#define RES_MIN_H 240
#define RES_MIN_W 320

#define RES_BASE_H 480
#define RES_BASE_W 640

#define RES_MAX_H 720
#define RES_MAX_W 960

#define EXPORT_IM_NAME "../Computed_plane.png"

#define SHRINK -1
#define ENLARGE 1

#ifndef SDL_EVENT_POLL_WAIT_MS
#define SDL_EVENT_POLL_WAIT_MS 10
#endif

typedef enum
{
   RES_MIN = 1,
   RES_BASE,
   RES_MAX,
} resolution;

void gui_init(void);
void gui_cleanup(void);
void gui_refresh(void);
void save_im(void);
void *win_thread(void *d);
resolution set_resolutin(resolution curr_res_type, int *h, int *v);

#endif