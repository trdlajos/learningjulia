
#include <stdio.h>
#include "../libs/utils.h"
#include "gui.h"
#include "computation.h"

struct
{

    // parameters of computation
    double c_re;
    double c_im;
    int num_iter;

    // ranges of complex plain
    double range_re_min;
    double range_re_max;
    double range_im_max;
    double range_im_min;

    // size of grid computed (in pixels)
    resolution res;
    int grid_w;
    int grid_h;
    uint8_t *grid;

    // cursor (position of computation in pixels)
    int cur_x;
    int cur_y;

    // increments of complex number
    double step_re;
    double step_im;

    // id, chunks
    int num_of_chunks;
    int curr_id;

    // dimensions of chunk(in pixels)
    uint8_t chunk_n_re;
    uint8_t chunk_n_im;

    // values of complex number at the start of chunk
    double chunk_start_re;
    double chunk_start_im;

    // control variables
    bool computing;
    bool computed;
    bool aborted;
    bool abort_request;
    bool set;

} comp = {

    .c_im = 0.6,
    .c_re = -0.4,
    .num_iter = 60,

    .range_im_min = -1.1,
    .range_im_max = 1.1,
    .range_re_max = 1.6,
    .range_re_min = -1.6,

    .res = RES_BASE,
    .grid = NULL,
    .grid_w = 640,
    .grid_h = 480,

    .computed = false,
    .abort_request = false,
    .aborted = false,
    .computing = false,
    .set = false,
};

/**
 * @brief allocates enough memory for grid,
 * computes steps of re and im variables for computation module,
 * determines number of chunks
 *
 */
void computation_init(void)
{
    comp.grid = allocate(comp.grid_h * comp.grid_w);

    comp.step_re = (comp.range_re_max - comp.range_re_min) / (1. * comp.grid_w);
    comp.step_im = -(comp.range_im_max - comp.range_im_min) / (1. * comp.grid_h);

    comp.chunk_n_im = comp.grid_h / 10;
    comp.chunk_n_re = comp.grid_w / 10;

    comp.num_of_chunks = (comp.grid_w * comp.grid_h) / (comp.chunk_n_re * comp.chunk_n_im);
    comp.set = false;
}

/**
 * @brief cleans memory
 *
 */
void computation_cleanup(void)
{

    if (comp.grid)
    {
        free(comp.grid);
    }
    comp.grid = NULL;
}

/**
 * @brief Prepares message with computation parameters to be send to computation module, resets buffer and computation
 *
 */
void set_compute(message *msg)
{

    msg->type = MSG_SET_COMPUTE;
    msg->data.set_compute.c_re = comp.c_re;
    msg->data.set_compute.c_im = comp.c_im;
    msg->data.set_compute.d_re = comp.step_re;
    msg->data.set_compute.d_im = comp.step_im;

    msg->data.set_compute.n = comp.num_iter;

    comp.set = true;

    // reset window in case pressed during computation or after finished computation
    if (comp.computing || comp.computed)
    {
        clear_buffer();
    }

    // log_mes(NOTE, __func__, __LINE__, __FILE__, "computation set msg init succes");
}

/**
 * @brief prepares message to comp_module specificating what needs to be computed
 *
 * @param msg - to be sent
 * @return true - succes
 * @return false - failure
 */
bool compute(message *msg)
{
    if (!is_computing())
    {
        // first chunk
        comp.curr_id = 0;
        comp.computing = true;
        comp.cur_x = comp.cur_y = 0; // start compute whole image
        comp.chunk_start_re = comp.range_re_min;
        comp.chunk_start_im = comp.range_im_max;
        msg->type = MSG_COMPUTE;
    }
    else
    {
        // next chunk
        comp.curr_id += 1;
        if (comp.curr_id < comp.num_of_chunks)
        {
            comp.cur_x += comp.chunk_n_re;
            comp.chunk_start_re += comp.chunk_n_re * comp.step_re;
            if (comp.cur_x >= comp.grid_w)
            {
                comp.cur_x = 0;
                comp.cur_y += comp.chunk_n_im;
                comp.chunk_start_re = comp.range_re_min;
                comp.chunk_start_im += comp.chunk_n_im * comp.step_im;
            }
            msg->type = MSG_COMPUTE;
        }
        else
        {
            log_mes(WARNING, __func__, __LINE__, __FILE__, "tried to compute chunk outside borders");
        }
    }

    // update message
    if (comp.computing && msg->type == MSG_COMPUTE)
    {
        msg->data.compute.cid = comp.curr_id;
        msg->data.compute.re = comp.chunk_start_re;
        msg->data.compute.im = comp.chunk_start_im;
        msg->data.compute.n_re = comp.chunk_n_re;
        msg->data.compute.n_im = comp.chunk_n_im;
    }

    return comp.computing;
}

/**
 * @brief process data computed in comp module(updates grid with computed iteration)
 *
 * @param compute_data
 */
void update_data(const msg_compute_data *compute_data)
{
    my_assert(compute_data != NULL, __func__, __LINE__, __FILE__);

    // log_mes(NOTE, __func__, __LINE__, __FILE__, "started updating data in computation");

    // check if chunk id correct
    if (compute_data->cid == comp.curr_id)
    {
        // compute position in grid(note that grid is actually 1d)
        const int idx = comp.cur_x + compute_data->i_re + (comp.cur_y + compute_data->i_im) * comp.grid_w;
        if (idx >= 0 && idx < (comp.grid_h * comp.grid_w))
        {
            comp.grid[idx] = compute_data->iter;
        }
        // check if last chunk has been computed
        if ((comp.curr_id + 1) >= comp.num_of_chunks && (compute_data->i_re + 1) == comp.chunk_n_re && (compute_data->i_im + 1) == comp.chunk_n_im)
        {
            comp.computed = true;
            comp.computing = false;
            // log_mes(NOTE, __func__, __LINE__, __FILE__, "last computation data recieved");
        }
    }
    else
    {
        log_mes(WARNING, __func__, __LINE__, __FILE__, "recieved chunk with unexpected chunk id");
    }
}

/**
 * @brief updates the image displayed according to data inside grid
 *
 */
void update_image(int w, int h, unsigned char *img)
{
    my_assert(img && comp.grid && w == comp.grid_w && h == comp.grid_h, __func__, __LINE__, __FILE__);

    for (int i = 0; i < w * h; ++i)
    {
        const double t = 1. * comp.grid[i] / (comp.num_iter + 1.0);
        *(img++) = 9 * (1 - t) * t * t * t * 255;
        *(img++) = 15 * (1 - t) * (1 - t) * t * t * 255;
        *(img++) = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
    }
}

/**
 * @brief clears all data obtained from previous computation
 *
 */
void clear_buffer()
{
    // ends computation
    comp.computing = false;
    comp.computed = false;
    comp.aborted = false;

    // clears grid
    for (int i = 0; i < comp.grid_h * comp.grid_w; i++)
    {
        comp.grid[i] = 0;
    }
    gui_refresh();
}

/**
 * @brief updates complex plane defining variables
 *
 * @param msg
 */
void update_complex_plane(message *msg)
{

    comp.range_im_max = msg->data.complex_plane.max_im;
    comp.range_re_max = msg->data.complex_plane.max_re;
    comp.range_im_min = msg->data.complex_plane.min_im;
    comp.range_re_min = msg->data.complex_plane.min_re;

    free(msg);

    computation_cleanup();
    computation_init();
    clear_buffer();
}

/**
 * @brief updates parameters of computation
 *
 */
void update_parameters(message *msg)
{
    comp.set = false;

    comp.c_im = msg->data.set_compute.c_im;
    comp.c_re = msg->data.set_compute.c_re;
    comp.num_iter = msg->data.set_compute.n;

    free(msg);
}

/**
 * @brief changes resolution of window, makes sure we stay in correct interval of desired resolutions
 *
 * @param how : ENLARGE(1) stands for higher resolution, SHRINK(-1) for smaller
 */
bool change_res(int how)
{
    clear_buffer();
    int curr_res = how + comp.res;
    if (curr_res < RES_MIN)
    {
        std_mes(WARNING, "Minimal resolution reached!");
        return false;
    }
    else if (curr_res > RES_MAX)
    {
        std_mes(WARNING, "Maximal resolution reached!");
        return false;
    }
    else
    {
        comp.res = set_resolutin(curr_res, &comp.grid_h, &comp.grid_w);
        std_mes(NOTE, "Resolution set to:");
        printf("%d %d\n", comp.grid_w, comp.grid_h);
    }
    computation_cleanup();
    computation_init();
    clear_buffer();
    return true;
}

// getters and setters

void get_grid_size(int *w, int *h)
{
    *h = comp.grid_h;
    *w = comp.grid_w;
}

bool is_computing(void)
{
    return comp.computing;
}

bool is_done(void)
{
    return comp.computed;
}

void request_abort(void)
{
    comp.abort_request = true;
}

bool is_abort_requested(void)
{
    return comp.abort_request;
}

void abort_computation(void)
{
    comp.abort_request = false;
    comp.aborted = true;
}

void enable_computation()
{
    comp.aborted = false;
}

bool is_aborted()
{
    return comp.aborted;
}

bool is_set(void)
{
    return comp.set;
}
