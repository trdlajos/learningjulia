
/*Main thread with implemented reading thread
 * Based on tutorial from prof. Faigl, with minor changes
 *
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include "../libs/event_queue.h"
#include "../libs/utils.h"
#include "../libs/prg_io_nonblock.h"
#include "main.h"
#include "keyboard.h"
#include "gui.h"

#define THRD_SUCCES 0
#define READ_TIMEOUT_MS 100

#define PIPE_IN_DEFAULT_NAME "/tmp/computational_module.out"
#define PIPE_OUT_DEFAULT_NAME "/tmp/computational_module.in"

void *read_pipe_thread(void *data);

int main(int argc, char const *argv[])
{
    int ret = EXIT_SUCCESS;

    // init pipe locations
    const char *pipe_in_loc = argc > 1 ? argv[1] : PIPE_IN_DEFAULT_NAME;
    const char *pipe_out_loc = argc > 2 ? argv[2] : PIPE_OUT_DEFAULT_NAME;

    int pipe_in = io_open_read(pipe_in_loc);
    int pipe_out = io_open_write(pipe_out_loc);

    my_assert(pipe_in != -1 && pipe_out != -1, __func__, __LINE__, __FILE__);

    enum
    {
        KEYBOARD_THRD,
        READ_PIPE_THRD,
        MAIN_THRD,
        WIN_THDR,
        NUM_THRDS
    };

    void *(*thrd_functions[])(void *) = {keyboard_thread, read_pipe_thread, main_thread, win_thread};
    const char *threads_names[] = {"Keyboard", "Pipe", "Main", "Win"};
    pthread_t threads[NUM_THRDS];

    void *thrd_data[NUM_THRDS] = {};
    thrd_data[READ_PIPE_THRD] = &pipe_in;
    thrd_data[MAIN_THRD] = &pipe_out;

    queue_init();

    for (int i = 0; i < NUM_THRDS; ++i)
    {
        int r = pthread_create(&threads[i], NULL, thrd_functions[i], thrd_data[i]);
        printf("Create thread '%s' %s\r\n", threads_names[i], (r == THRD_SUCCES ? "OK" : "FAIL"));
    }

    int ex;
    for (int i = 0; i < NUM_THRDS; ++i)
    {
        printf("Call join to the thread %s\r\n", threads_names[i]);
        int r = pthread_join(threads[i], (void *)&ex);
        printf("Joining the thread %s has been %s - exit value %i\r\n", threads_names[i], (r == THRD_SUCCES ? "OK" : "FAIL"), ex);
    }

    queue_cleanup();

    io_close(pipe_in);
    io_close(pipe_out);

    return ret;
}

/**
 * @brief reads an event from pipe ->
 * decides type of event(and message) ->
 * pushes event to queue
 *
 * @param data pointer to integer containing pipe
 * @return void* NULL
 */

void *read_pipe_thread(void *data)
{
    log_mes(NOTE, __func__, __LINE__, __FILE__, START);
    my_assert(data != NULL, __func__, __LINE__, __FILE__);

    int pipe_in = *(int *)data;
    bool end = false;
    uint8_t msg_buff[sizeof(message)];
    int i = 0;
    int mess_len = 0;

    // init queue
    queue_init();

    unsigned char c;

    // discard garbage from pipe
    // while (io_getc_timeout(pipe_in, IO_READ_TIMEOUT_MS, &c) > 0)
    // {}

    while (!end)
    {

        // read from pipe
        int r = io_getc_timeout(pipe_in, READ_TIMEOUT_MS, &c);
        if (r > 0)
        {
            // byte read

            if (i == 0)
            {
                // first byte has been read - decide nature of message
                if (get_message_size(c, &mess_len))
                {
                    msg_buff[i++] = c;
                }
                else
                {
                    log_mes(ERROR, __func__, __LINE__, __FILE__, "Unknown message type");
                }
            }
            else
            {
                // read rest of the message
                msg_buff[i++] = c;
            }

            if (mess_len > 0 && i == mess_len)
            {
                // all bytes of message have been read
                message *msg = (message *)allocate(sizeof(message));

                // debug
                //  log_mes(NOTE, __func__, __LINE__, __FILE__, "message has been read");

                if (parse_message_buf(msg_buff, mess_len, msg))
                {

                    // log_mes(NOTE, __func__, __LINE__, __FILE__, "message has been parsed");

                    event ev = {.type = EV_PIPE_IN_MESSAGE};
                    ev.data.msg = msg;

                    queue_push(ev);
                }
                else
                {
                    // could not parse message
                    //  log_mes(ERROR, __func__, __LINE__, __FILE__, "cannot parse message type");
                    free(msg);
                }
                i = mess_len = 0;
            }
        }
        else if (r == 0)
        {
            // timeout
        }
        else
        {
            // terminating program
            log_mes(ERROR, __func__, __LINE__, __FILE__, IO_ERROR_READING);
            set_quit();
            event ev = {.type = EV_QUIT};
            queue_push(ev);
        }
        end = is_quit();
    }

    log_mes(NOTE, __func__, __LINE__, __FILE__, END);
    return NULL;
}
