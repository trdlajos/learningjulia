/*Implementation of keyboard_thread
 *
 */
#include "../libs/event_queue.h"

#define READ_TIMEOUT_MS 100


void *keyboard_thread(void *data);
void decide_key_event(const int c, event *ev);
bool read_double(double *value, char *message);
int read_message(int max_len, char *message);
bool read_plane(event *ev);
bool read_param(event *ev);
