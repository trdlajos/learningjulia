
/**
 * @file main.c
 * @author JT
 * @brief Simple program that prints prime factorization on given input
 * @version 1
 * @date 2023-03-17
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

// error codes inicialization
typedef int Error;

enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_UNRECOGNIZED = 99,
    ERROR_BAD_INPUT,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Error not recognized!\n",
    "Error: Chybny vstup!\n",
};

// important macro init
#define GREATEST_PRIME 1000000
#define PRIME_ARRAY_LENGTH 78498


void report_error(int error);
int read_input(long *input);
int check_input(long *input);
int find_all_primes(int all_primes[], int length);
int find_prime_factorization(long number, int array[], int length);

int main(void){


    Error er;
    long input;
    
    //init array
    int primes[PRIME_ARRAY_LENGTH];
    primes[0]=2;
    primes[1]=3;

    //find all prime numbers
    er = find_all_primes(primes,PRIME_ARRAY_LENGTH);

    do{
        
        
        er = read_input(&input);
        
        // in case 0 inputted, end the program
        if(input==0)
          break;
        
        er == ERROR_OK ? er = check_input(&input) : er;
        er == ERROR_OK ? er = find_prime_factorization(input,primes,PRIME_ARRAY_LENGTH) : er;
    }

    while(er== ERROR_OK);
    report_error(er);
    return er;
}

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */

  if(error != ERROR_OK)
    (error >= ERROR_UNRECOGNIZED && error <= ERROR_BAD_INPUT) ? fprintf(stderr,"%s", error_strings[error-ERROR_UNRECOGNIZED]):fprintf(stderr,"%s",error_strings[0]);
  
}

int read_input(long *num){
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   * 
   */
  if(scanf("%ld",num)==1){
    return ERROR_OK;
  }
  else{
    // *num = 1;
    return ERROR_BAD_INPUT;
  }
}

int check_input(long *input){
  /**
   * @brief Checks if input is in correct value range
   * 
   */
  if(*input>0)
    return ERROR_OK;
  return ERROR_BAD_INPUT;
}

int find_all_primes(int primes[],int length){
  /**
   * @brief Finds all prime numbers between 1 and 1000000
   * 
   */

  //note that two basic primes are already in array - 2 and 3 
  int num_of_primes = 2;

  for(int num = 5; num < GREATEST_PRIME; num=num+2)
  {
    bool check = true;
    for(int divisor = 3; divisor < powf(num,0.5)+1; divisor = divisor + 2)
    {
      if(divisor == num)
        continue;

      if(num%divisor==0)
      {
        check = false;
        break;
      }
    }

    if(check)
    {
      primes[num_of_primes]=num;
      num_of_primes++;
    }
  }
  return ERROR_OK;
}

int find_prime_factorization(long number, int array[], int length){
  /**
   * @brief finds and prints all prime factors of given number
   * 
   */
  // defalut message
  printf("Prvociselny rozklad cisla %ld je:\n",number);
  
  // special case: 1
  if(number == 1){
    printf("1\n");
    return ERROR_OK;
  }

    
  long fraction = number;
  for(int i=0;i<length;i++)
  {
    short power = 0;
    int divisor = array[i];
    //find power
    while(fraction%divisor==0){
      fraction = fraction/divisor;
      power++;
    }

    //printing factors one by one, in case one is to bigger power than one, prints also the power
    if(power>0){
      printf("%d",divisor);
      if(power>1)
        printf("^%d",power);
      
      if(fraction<divisor)
      {
        printf("\n%lu fra\n",fraction);
        putchar('\n');
        break;
      }
      else
      {
        printf(" x ");
      }
    }
  }
  return ERROR_OK;
}



