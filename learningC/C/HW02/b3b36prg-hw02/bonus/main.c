
/**
 * @file main.c
 * @author JT
 * @brief Simple program that prints prime factorization on given input
 * @version 1
 * @date 2023-03-17
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

// error codes inicialization
typedef int Error;

enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_UNRECOGNIZED = 99,
  ERROR_BAD_INPUT,
  ERROR_MEM,
};

#define MEM_ERR_MESS "Error: Memory"

// error messages inicialization
static const char *error_strings[] = {
    "Error: Error not recognized!\n",
    "Error: Chybny vstup!\n",
};

// important macro init

#define GREATEST_PRIME 1000000
#define PRIME_ARRAY_LENGTH 78498

#define START_SIZE 50

void report_error(int error);
char *read_input(int *error, int *len);
int find_all_primes(int all_primes[], int length);
int find_prime_factorization(char *number, int prime_array[], int len);
bool divide_string_represented_num(char **num, int *len, int divisor);
void *mallocate(size_t size);
char *reallocate(char *arr, size_t old_size, size_t new_size);

int main(void)
{

  Error er;

  // init array
  int primes[PRIME_ARRAY_LENGTH];
  primes[0] = 2;
  primes[1] = 3;

  // find all prime numbers
  er = find_all_primes(primes, PRIME_ARRAY_LENGTH);

  do
  {
    int len = 0;
    char *arr = read_input(&er, &len);

    if (len == 1 && arr[0] == '0')
    {
      // end of file
      free(arr);
      break;
    }

    er == ERROR_OK ? er = find_prime_factorization(arr, primes, len) : er;

  } while (er == ERROR_OK);
  report_error(er);
  return er;
}

void report_error(int error)
{
  /**
   * @brief outputs right error message according to error number
   *
   */

  if (error != ERROR_OK)
    (error >= ERROR_UNRECOGNIZED && error <= ERROR_BAD_INPUT) ? fprintf(stderr, "%s", error_strings[error - ERROR_UNRECOGNIZED]) : fprintf(stderr, "%s", error_strings[0]);
}

// Should read string
char *read_input(int *error, int *len)
{
  /**
   * @brief reads large number as a string, if problem encounterd, write error, return NULL
   *
   */
  char c;
  int size = START_SIZE + 1; // do not forget \0;
  char *arr = mallocate(size * sizeof(char));
  int count = 0;
  while ((c = getchar()) != '\n' && c != EOF)
  {
    if (!(c >= '0' && c <= '9'))
    {
      *error = ERROR_BAD_INPUT;
      return NULL;
    }

    if (count == size)
    {
      int new_size = size * 2;
      arr = reallocate(arr, size, new_size + 1);
      size = new_size;
    }

    arr[count] = c;
    count++;
  }

  // ignore new lines

  arr[count] = '\0';
  *len = count;
  return arr;
}

int find_all_primes(int primes[], int length)
{
  /**
   * @brief Finds all prime numbers between 1 and 1000000
   *
   */

  // note that two basic primes are already in array - 2 and 3
  int num_of_primes = 2;

  for (int num = 5; num < GREATEST_PRIME; num = num + 2)
  {
    bool check = true;
    for (int divisor = 3; divisor < powf(num, 0.5) + 1; divisor = divisor + 2)
    {
      if (divisor == num)
        continue;

      if (num % divisor == 0)
      {
        check = false;
        break;
      }
    }

    if (check)
    {
      primes[num_of_primes] = num;
      num_of_primes++;
    }
  }
  return ERROR_OK;
}

// TODO CHANGE ALL
int find_prime_factorization(char *number, int prime_array[], int len)
{
  /**
   * @brief finds and prints all prime factors of given number
   *
   */
  // defalut message
  printf("Prvociselny rozklad cisla %s je:\n", number);

  // special case: 1
  if (len == 1 && number[0] == '1')
  {
    printf("1\n");
    free(number);
    return ERROR_OK;
  }


  for (int i = 0; i < PRIME_ARRAY_LENGTH; i++)
  {
    short power = 0;
    int divisor = prime_array[i];
    // find power
    // if(divisor>10){break;}

    while (divide_string_represented_num(&number, &len, divisor))
    {
      power++;
    }

    // printing factors one by one, in case one is to bigger power than one, prints also the power
    if (power > 0)
    {
      printf("%d", divisor);
      if (power > 1)
        printf("^%d", power);

      if (len == 1 && number[0] == '1')
      {
        // number factorized
        putchar('\n');
        free(number);
        return ERROR_OK;
      }
      else
      {
        printf(" x ");
      }
    }
  }
  printf("\nCould not find all divaders!\n");
  free(number);
  return ERROR_OK;
}

/**
 * @brief divides string with divisor
 *
 * @param num pointer to pointer to start of string represeted num /if ti is divisible,
 * changes to resolt of division, if not, stayes same
 *
 * @param divisor divisor diving the num
 * @return
 *  true if it is divisable by divisor
 *  false if it is not divisible
 */

bool divide_string_represented_num(char **num, int *len, int divisor)
{
  char *values = *num;

  // printf("DEBUG, %s %d %d\n", values, *len, divisor);

  char *result = mallocate(*len + 1);

  int tmp = 0;
  int index = 0;
  int new_len = 0;
  bool divisible = true;

  while (tmp < divisor)
  {
    if (index == *len)
    {
      divisible = false;
      break;
    }
    tmp = tmp * 10 + values[index] - '0';
    index++;
  }
  while (divisible)
  {
    // compute one digit
    result[new_len] = (int)tmp / divisor + '0';
    new_len++;

    // update tmp
    tmp = tmp % divisor;

    // printf("tmp: %d", tmp);

    if (index == *len && tmp == 0)
    {
      break;
    }
    else if (index == *len)
    {
      divisible = false;
      break;
    }
    // update tmp
    tmp = tmp * 10 + values[index] - '0';
    index++;
  }
  if (divisible)
  {
    free(values);
    result[new_len] = '\0';
    *num = result;
    *len = new_len;
    return true;
  }
  else
  {
    // divisor cannot divide
    free(result);
    // printf("Cannot divide!\n");
    return false;
  }
}

void *mallocate(size_t size)
{
  void *arr = malloc(size);
  if (arr == NULL)
  {
    fprintf(stderr, MEM_ERR_MESS);
    exit(ERROR_MEM);
  }
  return arr;
}

char *reallocate(char *arr, size_t old_size, size_t new_size)
{
  char *tmp = malloc(new_size);
  if (tmp == NULL)
  {
    free(arr);
    fprintf(stderr, MEM_ERR_MESS);
    exit(ERROR_MEM);
  }

  // copy values
  for (int i = 0; i < old_size; i++)
  {
    tmp[i] = arr[i];
  }

  free(arr);
  return tmp;
}
