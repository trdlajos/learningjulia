#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define BASIC_STR_LEN 20
#define COLOR_OPTION "--color=always"
#define COLOR_OPT_LEN 14
#define REGULAR_OPTION "-E"
#define REGULAR_OPT_LEN 2

// error codes inicialization
enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_NO_MATCH = 1,
  ERROR_FILE_NOT_FOUND = 100,
  ERROR_BAD_ARG,
  ERROR_BAD_OPT,
  ERROR_MEMORY,
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: File not found!\n",
    "Error: Arguments badly specified!\n",
    "Error: Options badly specified!\n",
    "Error: Memory could not be allocated!\n",
};

void report_error(int error);
int control(int argc, char *argv[]);

int decide_option(char *option, bool *has_color, bool *has_reg);
int open_file(FILE **file_ptr, char *file_name);
char *read_line(FILE *input, int *length, int *error);
bool line_contains(char *line, char *pattern, int line_len, int pattern_len, bool has_reg_opt);
int find_all_lines(char *pattern, FILE *file_ptr, bool has_color_opt, bool has_regular_opt);
int measure_str(char *string);
int pattern_contains_reg(char *pattern, int pattern_len, int *special_char_type);

void print_line(char *line, char *pattern, int line_len, int pattern_len, bool has_color_opt);

int main(int argc, char *argv[])
{
  int error = control(argc, argv);
  report_error(error);
  return error;
}

void report_error(int error)
{
  /**
   * @brief outputs right error message according to error number
   *
   */
  if (error >= ERROR_FILE_NOT_FOUND && error <= ERROR_MEMORY)
    fprintf(stderr, "%s", error_strings[error - ERROR_FILE_NOT_FOUND]);
}

int control(int argc, char *argv[])
{
  /**
   * @brief controls error handling and flow of program
   *
   */
  int error;
  FILE *file_ptr;

  char *pattern;
  bool has_color_opt = false;
  bool has_regular_opt = false;

  switch (argc)
  {
  case 1:
    error = ERROR_BAD_ARG;
    break;
  case 2:
    pattern = argv[1];
    file_ptr = stdin;
    error = ERROR_OK;
    break;
  case 3:
    pattern = argv[1];
    error = open_file(&file_ptr, argv[2]);
    break;
  case 4:
    error = decide_option(argv[1], &has_color_opt, &has_regular_opt);
    pattern = argv[2];
    if (error == ERROR_OK)
      error = open_file(&file_ptr, argv[3]);
    break;
  default:
    error = ERROR_BAD_ARG;
    break;
  }

  if (error == ERROR_OK)
  {
    error = find_all_lines(pattern, file_ptr, has_color_opt, has_regular_opt);
    fclose(file_ptr);
  }

  return error;
}

int open_file(FILE **file_ptr, char *file_name)
{
  /**
   * @brief opens a file
   *
   */
  *file_ptr = fopen(file_name, "r");
  if (*file_ptr == NULL)
  {
    return ERROR_FILE_NOT_FOUND;
  }
  return ERROR_OK;
}

char *read_line(FILE *input_file, int *length, int *error)
{
  /**
   * @brief reads a line from file
   *
   */

  int capacity = BASIC_STR_LEN;
  char *new_line = malloc(capacity + 1);
  if (new_line == NULL)
  {
    *error = ERROR_MEMORY;
    return NULL;
  }

  int input;
  int len = 0;

  while ((input = fgetc(input_file)) != EOF && input != '\n')
  {
    // realloc
    if (len == capacity)
    {
      capacity = capacity * 2;
      char *tmp = realloc(new_line, capacity + 1);
      if (tmp == NULL)
      {
        // realloc failure
        free(new_line);
        *error = ERROR_MEMORY;
        return NULL;
      }
      new_line = tmp;
    }

    new_line[len] = input;
    len++;
  }

  // end of file reached
  if (input == EOF && len == 0)
  {
    free(new_line);
    return NULL;
  }

  new_line[len] = '\0';
  *length = len;
  return new_line;
}

bool line_contains(char *line, char *pattern, int line_len, int pattern_len, bool has_regular_opt)
{
  /**
   * @brief checks if line contains desired pattern or not, if true, return 1, else 0
   *
   */
  int special_char_type = 0;
  int special_char_pos = 0;
  if (has_regular_opt)
    special_char_pos = pattern_contains_reg(pattern, pattern_len, &special_char_type);
  for (int i = 0; i < line_len; i++)
  {
    int j = 0;
    int line_offset = 0;
    int pattern_offset = 0;
    while (i + j + line_offset < line_len)
    {
      bool check = false;
      if (has_regular_opt && special_char_pos > 0 && j == special_char_pos - 1)
      {
        while (line[i + j + line_offset] == pattern[j + pattern_offset])
          line_offset++;
        switch (special_char_type)
        {
        case 1:
          break;
        case 2:
          if (line_offset == 0)
            check = true;
          break;
        case 3:
          if (line_offset > 1)
            check = true;
          break;
        }
        pattern_offset = 2;
      }

      if (j + pattern_offset < pattern_len && (line[i + j + line_offset] != pattern[j + pattern_offset] || check))
        break;
      j++;

      if (j + pattern_offset >= pattern_len)
        return true;
    }
  }
  return false;
}

int find_all_lines(char *pattern, FILE *file_ptr, bool has_color_opt, bool has_regular_opt)
{
  /**
   * @brief finds and prints all lines containing desired pattern in file
   *
   */
  int pattern_len = measure_str(pattern);
  int line_len;
  int error = ERROR_OK;
  bool found_match = false;

  do
  {
    char *line = read_line(file_ptr, &line_len, &error);
    if (line == NULL)
    {
      break;
    }
    else if (line_contains(line, pattern, line_len, pattern_len, has_regular_opt))
    {
      found_match = true;
      print_line(line, pattern, line_len, pattern_len, has_color_opt);
    }
    free(line);
  } while (1);

  if (error == ERROR_OK && found_match == false)
    return ERROR_NO_MATCH;
  return error;
}

int measure_str(char *string)
{
  /**
   * @brief Measures length of string
   *
   */
  int len = 1;
  while (*(string + len) != '\0')
  {
    len++;
  }
  return len;
}

void print_line(char *line, char *pattern, int line_len, int pattern_len, bool has_color_opt)
{
  if (has_color_opt)
  {
    for (int i = 0; i < line_len;)
    {
      int j = 0;
      bool check = false;
      while (j < pattern_len && i + j < line_len)
      {
        check = true;
        if (line[i + j] != pattern[j])
        {
          check = false;
          break;
        }
        j++;
      }
      if (check)
      {
        printf("\x1b[01;31m\x1b[K");
        int tmp = i;
        while (i < tmp + j)
        {
          putchar(line[i]);
          i++;
        }
        printf("\x1b[m\x1b[K");
      }
      else
      {
        putchar(line[i]);
        i++;
      }
    }
    putchar('\n');
  }
  else
  {
    printf("%s\n", line);
  }
}

int decide_option(char *option, bool *has_color, bool *has_reg)

{
  int option_len = measure_str(option);
  if (COLOR_OPT_LEN == option_len && line_contains(option, COLOR_OPTION, option_len, COLOR_OPT_LEN, false))
  {
    *has_color = true;
    return ERROR_OK;
  }
  else if (REGULAR_OPT_LEN == option_len && line_contains(option, REGULAR_OPTION, option_len, REGULAR_OPT_LEN, false))
  {
    *has_reg = true;
    return ERROR_OK;
  }
  return ERROR_BAD_OPT;
}

int pattern_contains_reg(char *pattern, int pattern_len, int *special_char_type)
{
  /**
   * @brief checks of what type and where in pattern is located special symbol
   * types of special chars:
   * 0 - without
   * 1 - *
   * 2 - +
   * 3 - ?
   *
   */

  *special_char_type = 0;
  for (int i = 0; i < pattern_len; i++)
  {
    switch (pattern[i])
    {
    case '*':
      *special_char_type = 1;
      return i;
    case '+':
      *special_char_type = 2;
      return i;
    case '?':
      *special_char_type = 3;
      return i;
    }
  }
  return 0;
}
