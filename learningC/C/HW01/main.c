/**
 * @file main.c
 * @author Josef Trdla
 * @brief Prints house according to requerements stated in HW01
 * @date 2023-03-03
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// values defining interval for correct width and height
#define MAX_VALUE 69
#define MIN_VALUE 3

// error numbers inicialization
enum
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_BAD_INP = 100,
  ERROR_BAD_INTERVAL = 101,
  ERROR_NOT_ODD = 102,
  ERROR_BAD_FENCE = 103

};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Vstup mimo interval!\n",
    "Error: Sirka neni liche cislo!\n",
    "Error: Neplatna velikost plotu!\n"};

void report_error(int error);
int read_input(int *num);
int read_double_input(int *num1, int *num2);
int check_house_dim(int width, int height);
bool check_odd(int num);
int check_fence(int fence, int height);
void print_roof(int width);
void print_house(int height, int width, bool using_fence, int fence);
bool print_inside(int width, int index, int height, bool check, bool using_fence);
void print_fence(int fence, int height, int index);

int main(int argc, char *argv[])
{
  int ret;
  int width;
  int height;
  int fence;

  bool using_fence = false;

  // reading variables width and height
  ret = read_double_input(&width, &height);

  // control if width and height are in right interval
  ret == ERROR_OK ? ret = check_house_dim(width, height) : ret;

  // control of odd width
  if (ret == ERROR_OK)
  {
    if (!check_odd(width))
    {
      ret = ERROR_NOT_ODD;
    }
  }

  // determining use of fence
  if (width == height)
  {
    using_fence = true;
  }

  // optional input - fence, in case width and height are same
  (ret == ERROR_OK && using_fence) ? ret = read_input(&fence) : ret;

  // controls if fence is correct
  (ret == ERROR_OK && using_fence) ? ret = check_fence(fence, height) : ret;

  if (ret == ERROR_OK)
  {
    print_roof(width);
    print_house(height, width, using_fence, fence);
  }
  else
  {
    report_error(ret);
  }

  return ret;
}

void report_error(int error)
{
  /**
   * @brief outputs right error message according to error number
   *
   */
  (error >= ERROR_BAD_INP && error <= ERROR_BAD_FENCE) ? fprintf(stderr, "%s", error_strings[error - ERROR_BAD_INP]) : fprintf(stderr, "ERROR: Spatne cislo erroru!");
}

int read_input(int *num)
{
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   *
   */
  if (scanf("%d", num) == 1)
  {
    return ERROR_OK;
  }
  else
  {
    return ERROR_BAD_INP;
  }
}

int read_double_input(int *num1, int *num2)
{
  /**
   * @brief Reads two inputs and stores them in num1 and num2 variables, also check if input was correct
   *
   */
  if (scanf("%d %d", num1, num2) == 2)
  {
    return ERROR_OK;
  }
  else
  {
    return ERROR_BAD_INP;
  }
}

int check_house_dim(int width, int height)
{
  /**
   * @brief Checks if dimensions of parameters are corect
   *
   */

  if ((MIN_VALUE <= width && width <= MAX_VALUE) && (MIN_VALUE <= height && height <= MAX_VALUE))
  {
    return ERROR_OK;
  }
  else
  {
    return ERROR_BAD_INTERVAL;
  }
}

bool check_odd(int num)
{
  /**
   * @brief Checks if inputed num is odd
   *
   */
  if ((num % 2) == 1)
  {
    return true;
  }
  else
  {
    return false;
  }
}

int check_fence(int fence, int height)
{
  /**
   * @brief checks if fence is in correct interval
   *
   */
  if (fence > 0 && fence < height)
  {
    return ERROR_OK;
  }
  else
  {
    return ERROR_BAD_FENCE;
  }
}

void print_roof(int width)
{
  /**
   * @brief Prints the roof without the house itself
   *
   */

  int height = width / 2; // heigth of the roof
  int right_pos;          // right position of the X measured from the middle of the roof
  int left_pos;           // left position of the X measured from the middle of the roof

  right_pos = left_pos = height + 1;

  for (int i = 1; i <= height; i++)
  {
    for (int j = 1; j <= right_pos; j++)
    {
      if (j == right_pos || j == left_pos)
      {
        printf("X");
      }
      else
      {
        printf(" ");
      }
    }
    printf("\n");
    right_pos += 1;
    left_pos -= 1;
  }
}

void print_house(int height, int width, bool using_fence, int fence)
{
  /**
   * @brief prints the house without roof
   *
   */

  bool check = false; // in case we need to print chars inside the house, this check
                      // determines what variable should be printed first, if o or *

  for (int i = 1; i <= height; i++)
  {
    // printing inside of the house
    check = print_inside(width, i, height, check, using_fence);

    // printing fence, in case there should be any
    if (using_fence && i >= height - fence + 1)
    {
      print_fence(fence, height, i);
    }

    printf("\n");
  }
}

bool print_inside(int width, int index, int height, bool check, bool using_fence)
{
  /**
   * @brief prints one row of insides of the house
   *
   */

  bool buffered_check = check; // determines the exact char to be printed

  // prints whole row char bu char
  for (int j = 1; j <= width; j++)
  {

    // printing X
    if (index == 1 || index == height || j == 1 || j == width)
    {
      printf("X");
    }
    else
    {
      if (using_fence)
      {

        // printing o / *
        if (buffered_check)
        {
          printf("o");
        }
        else
        {
          printf("*");
        }
        buffered_check = !buffered_check; // switches the char to be printed
      }

      // printing whitespace
      else
      {
        printf(" ");
      }
    }
  }
  // makes next row start with different char
  return !check;
}

void print_fence(int fence, int height, int index)
{
  /**
   * @brief prints fence around the house
   *
   */

  for (int j = fence; j > 0; j--)
  {
    if (check_odd(j))
    {
      printf("|");
    }
    // determining position of fence's borders
    else if (index == height || index == (height - fence + 1))
    {
      printf("-");
    }
    else
    {
      printf(" ");
    }
  }
}
