
#include <stdlib.h>
#include <stdio.h>

enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_UNRECOGNIZED = 99,
    ERROR_BAD_INPUT,
};

int read_input(int *num){
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   * 
   */
  int buff;
  if(scanf("%d",num)==1){
    return ERROR_OK;
  }
  else{
    *num = 1;
    return ERROR_BAD_INPUT;
  }
}

void print_array(int array[],int length){
    
    printf("\nPrinting...\n");

    for (int i = 0; i < length; i++)
    {
        printf("%d\n",array[i]);
    }
    
}

void print_array_as_matrix(int array[],int length,int matrix_dim){
      printf("\nPrinting...\n");

    int counter = 0;
    for (int i = 0; i < length; i++)
    {
      if(counter==matrix_dim){
        putchar('\n');
        counter=0;
        }
      
      printf("%2d",array[i]);
      counter++;
    }
    putchar('\n');
}

void print_array_as_histogram(int array[],int length)
/**
 * @brief prints array as histogram
 * 
 */
{
      printf("\nPrinting histogram\n");

    for (int i = 0; i < length; i++)
    { 

      //printing legend
      printf("[%d]: ",i);
      
      //printing #
      for(int j = 0; j<array[i];j++){
        putchar('#');
      }

      putchar('\n');
    }
    putchar('\n');
}


void init_array(int array[],int length){
  for (int i = 0; i < length; i++)
    {
      array[i]=0;
    }
}

int* create_rand_array(int len) {
/**
 * @brief creates array and fills it with random numbers 
 * 
 */

// warning: function returns address of local variable [-Wreturn-local-addr]
int arr[len]; 
//zivotnost toho pole jenom v te funkci (ulozeno na stacku)
// abychom mohli pracovat musime to pole ulozit na heapu -> dynamicka alokace
int *arr2 = (int*) malloc (sizeof(int) * len);
for (int i = 0; i < len; i++) {
arr2[i] = rand() % 10;

}
return arr2;
}

int* histogram(int *arr, int n) { 
/**
 * @brief takes an array and outputs an array containing number of occurances of givent item on index == item
 * 
 */

int bins = 10; 
// pocet binu v histogramu - pocet ruznych cisel v poli

//(mame ale dane pole od 0 do 9 -> takze 10)

int * hist = (int*) malloc (sizeof(int) * bins);
for (int i = 0; i < n; i++) {

hist[arr[i]]++;

}
return hist;

}


int main(){

    int length;
    scanf("%d",&length);

    int *array;
    array = create_rand_array(length);
    int *hist = histogram(array,length);
    print_array_as_histogram(hist,10);

    /*
    takes std input, adds it to array, at the and prints array, note that you need to declare first!
    while (counter < length)
    {
        int input;
        read_input(&input);
        *(array+counter) = input;
        counter++;
    }
    print_array_as_histogram(array,length);
    */
    }



