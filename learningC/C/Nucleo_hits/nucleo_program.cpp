#include "mbed.h"
#include <cstdint>

BufferedSerial serial(USBTX,USBRX,112500);
Ticker timer;
DigitalOut myled(LED1);
DigitalIn mybutton(BUTTON1);



void blink(int count, Kernel::Clock::duration_u32 t=200ms){
  for(uint32_t i=0;i<count;++i){
    ThisThread::sleep_for(t);
    myled=1;
    ThisThread::sleep_for(t);
    myled=0;
  }
}

void on_timer(){
    char response='f';
    myled=!myled;
    serial.write(&response,1);
}

int main(){
    serial.set_blocking(true);
    
    //send start char
    const char init_mess='i';
    serial.write(&init_mess,1);
   
    blink(5);
    while(true){
        char incom_mes;
        int r = serial.read(&incom_mes,1); 
        char response='a';
        if(r==1){
            switch(incom_mes){
                case 's': //start
                    timer.detach();
                    myled=1;
                    break;
                case 'e': // end 
                    timer.detach();
                    myled=0;
                    break;
                case 'f': // change of state
                    timer.detach();
                    myled= ! myled;
                    break;
                case '0': // abort 
                    timer.detach();
                    break;
                case '1': // various frequences
                    timer.attach(&on_timer,50ms);
                    break;
                case '2':
                    timer.attach(&on_timer,100ms);
                    break;
                case '3':
                    timer.attach(&on_timer,200ms);
                    break;
                case '4':
                    timer.attach(&on_timer,500ms);
                    break;
                case '5':
                    timer.attach(&on_timer,1s);
                    break;
                default:
                    response='!';
                    break;
            }
        }else{
            response='!';
        }
        serial.write(&response,1); //1 byte
    }
}
