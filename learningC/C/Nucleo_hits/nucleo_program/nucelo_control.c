#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>

#include "prg_serial.h"

void handler(int signal_code);
void set_raw(_Bool set);


int main(int argc, char *argv[])
{
    signal(SIGINT, &handler);
    int ret = 0;
    char c;
    const char *serial = argc > 1 ? argv[1] : "/dev/ttyACM0"; // not sure this is the right port
    int fd = serial_open(serial);
    if (fd != -1)
    {
        set_raw(1);
        bool quit = false;
        bool write_read_response = false;
        while (!quit)
        {
            c = getchar();
            switch (c)
            {
            case 'q':
                printf("Quit the program\n");
                quit = true;
                break;
            case 's':
                printf("Send 's' - LED on\n");
                write_read_response = true;
                break;
            case 'e':
                printf("Send 'e' - LED off\n");
                write_read_response = true;
                break;
            case 'f':
                printf("Send 'f' - LED switch\n");
                write_read_response = true;
                break;
            default:
                printf("Ignoring char '%d'\n", c);
                break;
            }
            if (write_read_response)
            {
                int r = serial_putc(fd, c);
                if (r != -1)
                {
                    fprintf(stderr, "DEBUG: Received response '%d'\n", r);
                }
                else
                {
                    fprintf(stderr, "ERROR: Error in received responses\n");
                }

                quit = c == 'q';
            }
        } // end while()

        serial_close(fd);
        set_raw(0);
    }
    else
    {
        fprintf(stderr, "ERROR: Cannot open device %s\n", serial);
    }
    return ret;
}

void set_raw(_Bool set)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (set)
    {                 // put the terminal to raw
        tioOld = tio; // backup
        cfmakeraw(&tio);
        tio.c_lflag &= ~ECHO; // assure echo is disabled
        tio.c_oflag |= OPOST; // enable output postprocessing
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
    else
    { // set the previous settingsreset
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
}

void handler(int signal_code) {
  set_raw(0);
  exit(0);
}