#include <stdio.h>
#include <stdlib.h>


/**
 * @file main.c
 * @author JT
 * @brief LAB4 Notes - arrays
 * @version 0.*
 * @date 2023-03-15
 * 
 * @copyright Copyright (c) 2023
 * 
 */

// error messages inicialization
static const char *error_strings[] = {
    "Error: Error not recognized!\n",
    "Error: Input error!\n",
    "Error: Choice not in range\n",
    "Error: Runtime error\n"
};

// error codes inicialization
enum error {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_UNRECOGNIZED = 100,
    ERROR_BAD_INPUT,
    ERROR_CHOICE_OUT_OF_RANGE,
    ERROR_PROGRAM_FAIL,
};

int read_input(int *num);



int main(){

    int inputed_Numbers[10];
    int input;

    for (int i = 0; i < 10; i++)
    {
        read_input(&input);
        inputed_Numbers[i] = input; // before inicialization, all variables inside array are set to 0touch


    }

    for (int i = 0; i < 9; i=i+2)
    {
        printf("\nSUM: %d\n",inputed_Numbers[i]+inputed_Numbers[i+1]);
    }
    
    

    return 0;
}

int read_input(int *num){
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   * 
   */
  int buff;
  if(scanf("%d",num)==1){
    return ERROR_OK;
  }
  else{
    *num = 1;
    return ERROR_BAD_INPUT;
  }
}