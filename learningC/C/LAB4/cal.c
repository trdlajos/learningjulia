#include <stdio.h>
#include <stdlib.h>


static char *day_of_week[] = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" };
#define DAYS 7

static char *name_of_month[] = {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
};
static int days_in_month[] = { 31, 28, 31, 30, 31, 30,
                               31, 31, 30, 30, 30, 31 };
#define MONTHS 12

static int first_day_in_march = 1; // 1. 3. 2022 is Tuesday
static int first_day_in_year = 5; // 1. 1. 2022 is Saturday

void print_days_table(){
    for (int i = 0; i < DAYS; i++)
    {
        printf(" %s",day_of_week[i]);
    }
    putchar('\n');
}

void print_days_date(int start, int lenght){
    int counter = -start+1;
    
    while(counter<lenght){
        for (int i = 0; i < 7; i++)
        {
            if(counter<1){
                printf("   ");
            }
            else if(counter>lenght)
            {
                break;
            }
            else
            {
                printf(" %2d",counter);
            }
            counter++;
        }
        putchar('\n');
    }
}

void print_month(int month, int start_day){
    printf("%s\n",name_of_month[month]);
    print_days_table();
    print_days_date(start_day,days_in_month[month]);
}

void print_months_in_row(int start_month, int start_day, int length){
    int counter = 0;
    int offset;

    while(counter<length){
        
        if(start_month+counter > MONTHS){
            start_month = start_month - 12;
        }

        print_month(start_month+counter,start_day);
        putchar('\n');
        counter++;

        start_day = (2+start_day)%7;


    }
}

int main(){
    print_months_in_row(8,3,3);
    return 0;
}


