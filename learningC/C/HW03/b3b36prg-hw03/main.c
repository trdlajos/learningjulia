/**
 * @file main.c
 * @author JT
 * @brief Simple text decoder
 * @version 1
 * @date 2023-03-29
 *
 */

#include <stdio.h>
#include <stdlib.h>

#define BASIC_STR_LEN 10
#define NUMBER_OF_LETTERS 52

// error codes inicialization
enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_BAD_INPUT = 100,
  ERROR_INPUT_OUT_OF_RANGE,
  ERROR_REALLOC,
  ERROR_MALLOC
};

// error messages inicialization
static const char *error_strings[] = {
    "Error: Chybny vstup!\n",
    "Error: Chybna delka vstupu!\n",
    "Error: Chyba realloc\n",
    "Error: Chyba malloc\n",

};

// control
int control_error();

// input
char *read_message(int *length, int *error);

char rotate(char c);
char *shift(char *message, int message_len);
int compare(char *message, char *opd_text, int length);
char *decode(char *message, char *odp_text, int length);
char *copy_str(char *message, int length);

// output
void report_error(int error);
void print_array(char *array, int length);

int main()
{
  int error = control_error();
  report_error(error);
  return error;
}

int control_error()
{
  char *mess;
  char *eavas_text;
  int error = ERROR_OK;

  // reading message
  int mess_length;
  mess = read_message(&mess_length, &error);
  if (error != ERROR_OK)
    return error;

  // reading eavas_text
  int evas_text_length;
  eavas_text = read_message(&evas_text_length, &error);
  if (error != ERROR_OK)
    return error;

  // comparing values length of inputted strings
  if (mess_length != evas_text_length)
  {
    free(mess);
    free(eavas_text);
    return ERROR_INPUT_OUT_OF_RANGE;
  }

  char *result = decode(mess, eavas_text, mess_length);
  print_array(result, mess_length);
  putchar('\n');
  free(mess);
  free(eavas_text);
  free(result);
  return ERROR_OK;
}

char *read_message(int *length, int *error)
{
  /**
   * @brief reads a line terminated by \n character (that is not included in resulting message)
   *
   */

  int capacity = BASIC_STR_LEN;
  *error = ERROR_OK;

  // initalizes pointer to place where we will read our message
  char *input_message = malloc(capacity);
  if (input_message == NULL)
  {
    *error = ERROR_MALLOC;
    return NULL;
  }

  char *tmp;
  char input;
  int len = 0;

  // reading until bad character or end of line encoutered
  while ((input = getchar()) != EOF && input != '\n')
  {
    if ((input >= 'a' && input <= 'z') || (input >= 'A' && input <= 'Z'))
    {
      if (len == capacity - 1)
      {
        capacity = capacity * 2;
        tmp = realloc(input_message, capacity);
        if (tmp == NULL)
        {
          // realloc failure
          free(input_message);
          *error = ERROR_REALLOC;
          return NULL;
        }
        input_message = tmp;
      }
      *(input_message + len) = input;
      len++;
    }
    else
    {
      // bad input
      *error = ERROR_BAD_INPUT;
      free(input_message);
      return NULL;
    }
  }

  // in case nothing inputted
  if (len == 0)
  {
    *error = ERROR_BAD_INPUT;
    free(input_message);
    return NULL;
  }

  *length = len;
  return input_message;
}

char rotate(char c)
{
  /**
   * @brief increments the ascii value of character by one
   *
   */
  if (c == 'z')
    return 'A';
  if (c == 'Z')
    return 'a';

  return c + 1;
}

char *shift(char *message, int message_len)
{
  /**
   * @brief Increments ascii value of whole string by one
   *
   */
  for (int i = 0; i < message_len; i++)
  {
    message[i] = rotate(message[i]);
  }
  return message;
}
int compare(char *message, char *opd_text, int length)
{
  /**
   * @brief compares two strings, outputs number of same characters on same position in string
   *
   */

  int num = 0;
  for (int i = 0; i < length; i++)
  {
    if (message[i] == opd_text[i])
      num++;
  }
  return num;
}

char *decode(char *message, char *odp_text, int length)
{
  /**
   * @brief decodes message according to its greatest match with eavesdropp
   *
   */
  int best_value = compare(message, odp_text, length);
  char *best_result = copy_str(message, length);
  int tmp;
  for (int i = 0; i < NUMBER_OF_LETTERS; i++)
  {
    shift(message, length);
    tmp = compare(message, odp_text, length);
    if (tmp > best_value)
    {
      best_value = tmp;
      free(best_result);
      best_result = copy_str(message, length);
    }
  }
  return best_result;
}

char *copy_str(char *message, int length)
{
  /**
   * @brief makes a copy of inputted message
   *
   */
  char *copied = malloc(length);
  for (int i = 0; i < length; i++)
  {
    copied[i] = message[i];
  }
  return copied;
}

void print_array(char *array, int length)
/**
 * @brief prints whole string
 *
 */
{
  for (int i = 0; i < length; i++)
  {
    putchar(array[i]);
  }
}
