/**
 * @file simple_triangle_creator.c
 * @author JT
 * @brief does simple triangle
 * @version 0.1
 * @date 2023-03-08
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// error messages
enum {
    ERROR_OK = EXIT_SUCCESS,
    ERROR_BAD_INP  = 100,
    ERROR_BAD_RANGE = 101,
};

// interval specification

#define MIN_VAL 1
#ifndef MAX_VAL
#define MAX_VAL 100
#endif


// definiton of fill
#define FILL '*'

// error messages inicialization
static const char *error_strings[] = {
  "Error: Chybny vstup!\n",
  "Error: Vstup mimo interval!\n",
};

void report_error(int error){
  /**
   * @brief outputs right error message according to error number
   * 
   */
  (error >= ERROR_BAD_INP && error <= ERROR_BAD_RANGE) ? fprintf(stderr,"%s", error_strings[error-ERROR_BAD_INP]):fprintf(stderr,"ERROR: Spatne cislo erroru!");
  
}

int read_input(int *input);
int check_interval(int input);
void do_triangle(int height, char fill);
void print_line(int length, char fill);

int main(){

    //declaration
    int error = ERROR_OK;
    char fill = '*';
    int height;

    // reading, checking, printing
    error = read_input(&height);
    error == ERROR_OK ? error = check_interval(height):error;
    error == ERROR_OK ? do_triangle(height,FILL): report_error(error);

    //end
    return error;

}

int read_input(int *input){
  /**
   * @brief Reads input and stores it in num variable, also check if input was correct
   * 
   */
  if(scanf("%d",input)==1){
    return ERROR_OK;
  }
  else{
    return ERROR_BAD_INP;
  }
}

int check_interval(int input){
  /**
   * @brief Checks if dimensions of parameters are corect
   * 
   */

  if(MIN_VAL <= input && input <= MAX_VAL){
    return ERROR_OK;
  }else{
    return ERROR_BAD_RANGE;
  }
}

void do_triangle(int height, char fill){
    for(int i = 0; i<height; i++)
    {
        print_line(height-i,fill);
    }


}

void print_line(int length, char fill){
    for(int i = 0; i<length; i++)
    {
        putchar(fill);
    }
    putchar('\n');
}





