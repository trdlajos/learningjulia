/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 *
 * První vlákno je určeno pro zpracování vstupu (čtení stisknuté klávesy),
 * druhé vlákno aktualizuje výstup (jednořádkový) a třetí vlákno implementuje časovač,
 * který po uplynutí definované periody zvýší hodnotu proměnné (čítače).
 * Perioda může být nastavována uživatelem, stiskem definovaných kláves.
 *
 * @version 0.1
 * @date 2023-04-19
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <stdio.h>
#include <stdbool.h>

#include <unistd.h>
#include <pthread.h>

void *thread1(void *);
void *thread2(void *);

int counter;
bool quit = false;

pthread_mutex_t mtx; // mutex making sure that only one thread can work with variable at given time

pthread_cond_t condvar; // conditional variable for working with threads

int main(void)
{

   pthread_mutex_init(&mtx, NULL);
   pthread_cond_init(&condvar, NULL);

   pthread_t thrs[2];
   pthread_create(&thrs[0], NULL, thread1, NULL);
   pthread_create(&thrs[1], NULL, thread2, NULL);

   counter = 0;

   // locking variable

   getchar();

   pthread_mutex_lock(&mtx);
   quit = true;
   pthread_mutex_unlock(&mtx);

   for (int i = 0; i < 2; ++i)
   {
      pthread_join(thrs[i], NULL); // exit ends thread instantly(error...), join is used standartly
   }
   return 0;
}

void *thread1(void *v)
/**
 * @brief updates counter
 *
 */
{
   bool q = false;
   while (!q)
   {

      usleep(100 * 1000); // param in microsec

      // locking
      pthread_mutex_lock(&mtx);

      counter += 1;
      pthread_cond_signal(&condvar); // sends a signal

      q = quit;
      pthread_mutex_unlock(&mtx);
   }
   return 0;
}

void *thread2(void *v)
/**
 * @brief prints values of counter
 *
 */
{
   bool q = false;
   while (!q)
   {
      pthread_mutex_lock(&mtx);
      pthread_cond_wait(&condvar, &mtx); // waits for signal, stops this thread until signal receaved

      printf("\rCounter %10i", counter);

      fflush(stdout);

      q = quit;
      pthread_mutex_unlock(&mtx);
   }
   return 0;
}
