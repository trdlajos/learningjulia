#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define BASIC_ARR_LEN 20
#define MEM_ERROR_MESSAGE "Error: Chyba paměti!\n"
#define BAD_INP_ERROR_MESSAGE "Error: Chybny vstup!\n"
#define PLUS 0
#define MINUS 1
#define MULTIPLY 2

// error codes inicialization
enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_BAD_INP = 100,
  ERROR_MEMORY,
};

typedef struct matrix
{
  int rows;
  int cols;
  int *values;
} matrix;

void print_matrix(const matrix *const m);
matrix *allocate_matrix(void);
matrix *read_matrix(void);
void *mallocate(size_t size);
int read_dim(int *row, int *col);
void free_matrix(matrix *m);
matrix *add_matrix(matrix *m1,matrix *m2,bool subtract);
matrix *multiply_matrix(matrix *mat1, matrix *mat2);
int read_operation(void);
matrix *compute_matrix(matrix *mat1, matrix *mat2,int operation);

