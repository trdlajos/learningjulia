
#include "matrix.h"

void print_matrix(const matrix *const m)
{
  /**
   * @brief prints a matrix
   *
   */
  printf("%d %d\n", m->rows, m->cols);
  if (m && m->values)
  {
    for (int r = 0; r < m->rows; ++r)
    {
      for (int c = 0; c < m->cols; ++c)
      {
        printf("%d", m->values[r * m->cols + c]);
        if (c < m->cols - 1)
          putchar(' ');
      }
      putchar('\n');
    }
  }
  else
  {
    printf("%s", BAD_INP_ERROR_MESSAGE);
  }
}

int read_dim(int *row, int *col)
/**
 * @brief reads dimension of matrix
 *
 */
{
  if (scanf("%d %d", row, col) == 2)
  {
    return ERROR_OK;
  }
  return ERROR_BAD_INP;
}

void *mallocate(size_t size)
{
  /**
   * @brief Tries to allocate memory of given size, in case of failure terminates program
   *
   */
  void *ret = malloc(size);
  if (!ret)
  {
    fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
    exit(-1);
  }
  return ret;
}

matrix *allocate_matrix(void)
{
  /**
   * @brief allocates space for matrix struct
   *
   */
  matrix *ret = mallocate(sizeof(matrix));
  ret->cols = ret->rows = 0;
  ret->values = NULL;
  return ret;
}

void free_matrix(matrix *m)
/**
 * @brief frees all space allocated for given matrix
 *
 */
{
  if (m)
  {
    if (m->values)
      free(m->values);
    free(m);
  }
}

matrix *read_matrix(void)
/**
 * @brief reads a matrix from stdin
 *
 */
{
  matrix *ret = allocate_matrix();
  if (read_dim(&(ret->rows), &(ret->cols)))
  {
    free_matrix(ret);
    return NULL;
  }

  ret->values = mallocate(sizeof(int) * ret->cols * ret->rows);

  for (int i = 0; i < ret->cols * ret->rows; i++)
  {
    if (scanf("%d", &(ret->values[i])) != 1)
    {
      free_matrix(ret);
      return NULL;
    }
  }

  return ret;
}

matrix *add_matrix(matrix *mat1, matrix *mat2, bool subtract)
{
  /**
   * @brief adds two matrices together, result is stored in memory of mat1,
   * mat2 will be freed after the computation
   *
   */
  if (mat1->cols != mat2->cols || mat1->rows != mat2->rows)
  {
    free_matrix(mat1);
    free_matrix(mat2);
    return NULL;
  }

  for (int i = 0; i < mat1->cols * mat1->rows; i++)
  {
    if (subtract)
    {
      mat1->values[i] = mat1->values[i] - mat2->values[i];
    }
    else
    {
      mat1->values[i] = mat1->values[i] + mat2->values[i];
    }
  }

  free_matrix(mat2);
  return mat1;
}

matrix *multiply_matrix(matrix *mat1, matrix *mat2)
{
  /**
   * @brief multiplies two matrices, allocates space for new matrix, frees the two
   *
   */
  if (mat1->cols != mat2->rows)
  {
    free_matrix(mat1);
    free_matrix(mat2);
    return NULL;
  }

  matrix *result = allocate_matrix();
  result->values = mallocate(sizeof(int) * mat1->rows * mat2->cols);
  result->rows = mat1->rows;
  result->cols = mat2->cols;

  for (int r = 0; r < result->rows; r++)
  {
    for (int c = 0; c < result->cols; c++)
    {
      result->values[r * result->cols + c] = 0;
      for (int k = 0; k < mat1->cols; k++)
      {
        result->values[r * result->cols + c] += mat1->values[r * mat1->cols + k] * mat2->values[k * mat2->cols + c];
      }
    }
  }

  free_matrix(mat1);
  free_matrix(mat2);
  return result;
}

int read_operation(void)
{ /**
   * @brief reads a char denoting desired operation, outputs code according to table below,
   also check if EOF has been reached or no
   *  asci:43 + = 0
   *  asci:45 - = 1
   *  asci:42  * = 2
   */
  char c;
  do
  {
    int check = scanf("%c", &c);
    if (check == EOF)
    {
      return EOF;
    }
    else if (check != 1)
    {
      return ERROR_BAD_INP;
    }
  } while (c == '\n' || c == ' ');

  switch (c)
  {
  case 43:
    return PLUS;
    break;
  case 45:
    return MINUS;
    break;
  case 42:
    return MULTIPLY;
    break;
  }
  return ERROR_BAD_INP;
}

matrix *compute_matrix(matrix *mat_a, matrix *mat_b, int operation)
{ /**
   * @brief decides and performs operation between two matrices
   *
   */
  switch (operation)
  {
  case PLUS:
    mat_a = add_matrix(mat_a, mat_b, false);
    break;
  case MINUS:
    mat_a = add_matrix(mat_a, mat_b, true);
    break;
  case MULTIPLY:
    mat_a = multiply_matrix(mat_a, mat_b);
    break;
  default:
    return NULL;
  }

  if (mat_a == NULL)
  {
    return NULL;
  }

  return mat_a;
}
