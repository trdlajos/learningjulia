
#include "matrix.h"

int control(void);

/* The main program */
int main(int argc, char *argv[])
{
  int error = control();

  if (error == ERROR_BAD_INP)
  {
    fprintf(stderr, "%s", BAD_INP_ERROR_MESSAGE);
  }

  return error;
}

int control(void)
{
  /**
   * @brief controls run of whole program
   *
   */

  matrix *mat_a = NULL;
  matrix *mat_b = NULL;

  int ab_operation;
  int bc_operation;

  // read first two matrices A and B and their operation
  mat_a = read_matrix();
  if (mat_a == NULL)
    return ERROR_BAD_INP;

  ab_operation = read_operation();
  if (ab_operation == ERROR_BAD_INP || ab_operation == EOF)
  {
    free_matrix(mat_a);
    return ERROR_BAD_INP;
  }

  mat_b = read_matrix();
  if (mat_b == NULL)
  {
    free_matrix(mat_a);
    return ERROR_BAD_INP;
  }

  // reads matrices one by one, until EOF reached.
  while ((bc_operation = read_operation()) != EOF)
  {
    if (bc_operation == ERROR_BAD_INP)
    {
      free_matrix(mat_a);
      free_matrix(mat_b);
      return ERROR_BAD_INP;
    }

    // read third matrix C
    matrix *mat_c = read_matrix();
    if (mat_c == NULL)
    {
      free_matrix(mat_a);
      free_matrix(mat_b);
      return ERROR_BAD_INP;
    }

    // if newly-find operation is multiply, we multiply matrices B and C,
    // if it is another operation, we do the operation between A and B
    if (bc_operation == MULTIPLY)
    {
      mat_b = compute_matrix(mat_b, mat_c, bc_operation);
      if (mat_b == NULL)
      {
        return ERROR_BAD_INP;
      }
    }
    else
    {
      mat_a = compute_matrix(mat_a, mat_b, ab_operation);
      if (mat_a == NULL)
      {
        return ERROR_BAD_INP;
      }
      ab_operation = bc_operation;
      mat_b = mat_c;
    }
  }

  mat_a = compute_matrix(mat_a, mat_b, ab_operation);
  if (mat_a == NULL)
  {
    return ERROR_BAD_INP;
  }
  print_matrix(mat_a);
  free_matrix(mat_a);
  return ERROR_OK;
}
