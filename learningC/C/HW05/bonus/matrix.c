
#include "matrix.h"

// UTILS PART //

void print_matrix(const matrix *const m)
{

  if (m && m->values)
  {
    putchar('[');
    for (int r = 0; r < m->rows; ++r)
    {
      for (int c = 0; c < m->cols; ++c)
      {
        printf("%d", m->values[r * m->cols + c]);
        if (c < m->cols - 1)
          putchar(' ');
      }
      if (r != m->rows - 1)
      {
        putchar(';');
        putchar(' ');
      }
    }
    printf("]\n");
  }
  else
  {
    fprintf(stderr,"PM - %s", BAD_INP_ERROR_MESSAGE);
  }
}

void print_matrices(matrix **matrices)
{
  for (int i = 0; i < MATRICES_LEN; i++)
  {
    if (matrices[i])
    {
      char letter = i + 'A';
      printf("Matrix %c:\n",letter);
      print_matrix(matrices[i]);
    }
  }
}

matrix *copy_matrix(matrix *m)
{
  matrix *copied = allocate_matrix(m->cols, m->rows);
  int *copied_values = mallocate(m->cols * m->rows, sizeof(int));
  for (int i = 0; i < m->cols * m->rows; i++)
  {
    copied_values[i] = m->values[i];
  }
  copied->values = copied_values;
  return copied;
}

int get_mat_index(int letter)
{
  return letter - 'A';
}

// MEMORY PART //

void *mallocate(size_t num_of_items, size_t size)
{
  /**
   * @brief Tries to allocate memory of given size, in case of failure terminates program
   *
   */
  void *ret = calloc(num_of_items, size);
  if (!ret)
  {
    fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
    exit(ERROR_MEMORY);
  }
  return ret;
}

int *reallocate(int *values, int old_len, int new_len)
{
  // realloc
  int *tmp = mallocate(new_len, sizeof(int));
  if (tmp == NULL)
  {
    // realloc failure
    free(values);
    exit(ERROR_MEMORY);
  }
  // copy values
  for (int i = 0; i < old_len; i++)
  {
    tmp[i] = values[i];
  }
  free(values);
  return tmp;
}

matrix *allocate_matrix(int cols, int rows)
{
  /**
   * @brief allocates space for matrix struct, init cols to START_LEN
   *
   */
  matrix *ret = mallocate(1, sizeof(matrix));
  ret->cols = cols;
  ret->rows = rows;

  return ret;
}

void free_matrix(matrix *m)
/**
 * @brief frees all space allocated for given matrix
 *
 */
{
  if (m)
  {
    if (m->values)
      free(m->values);
    free(m);
  }
}

// READING PART //

bool read_line(matrix **matrices, int *error)
{
  int read_matrix_error = ERROR_OK;
  int letter = getchar();
  if (letter >= 'A' && letter <= 'Z')
  {
    int operation = getchar();

    if (operation == '=')
    {
      if (getchar() != '[')
      {
        *error = ERROR_BAD_INP;
        return true;
      }

      int index = letter - 'A';
      matrices[index] = read_matrix(&read_matrix_error);
    }
    else
    {
      matrix *res = get_result(matrices, operation, letter, error);
      if (*error == ERROR_OK)
      {
        print_matrix(res);
        free_matrix(res);
      }

      return true;
    }
  }
  else if (letter == '\n')
  {
    // continue
  }
  else if (letter == EOF)
  {
    // end of file - terminate
    return true;
  }
  else
  {
    *error = ERROR_BAD_INP;
    return true;
  }

  return false;
}

matrix *read_matrix(int *error)
/**
 * @brief reads a matrix from stdin
 *
 */
{
  int row_error = ERROR_OK;
  int rows;
  int cols;
  rows = cols = START_LEN_VALUE;
  int *values = (int *)mallocate(cols, sizeof(int));
  while (!read_row(&values, &cols, rows, &row_error))
  {
    // make room for another row
    rows += 1;
    values = reallocate(values, cols * (rows - 1), cols * rows);
  }

  *error = row_error;
  matrix *ret = allocate_matrix(cols, rows);
  ret->values = values;

  return ret;
}

/**
 * @brief reads a row to values matrix (1d), cols == len of row
 * return true if finished reading matrix, else false
 *
 */
bool read_row(int **ptr_values, int *cols, int rows, int *error)
{
  int value;
  int cur_col = 0;
  int *values = *ptr_values;
  while (scanf("%d", &value) == 1)
  {

    values[*cols * (rows - 1) + cur_col] = value;
    cur_col += 1;

    int c = getc(stdin);

    if (c == ']')
    {
      return true;
    }
    else if (c != ' ')
    {
      // wrong len of row
      if (cur_col != *cols)
      {
        *error = ERROR_BAD_INP;
        return true;
      }
      return false;
    }

    // if reading first row, adjust len of row - cols!
    if (rows == 1 && cur_col == *cols)
    {
      *cols = cur_col + 1;
      values = reallocate(values, cur_col, *cols);
      *ptr_values = values;
    }
  }
  *error = ERROR_BAD_INP;
  return true;
}

// HANDLING STORAGE OF MATRICES //

matrix **create_list_of_matrices(void)
{
  matrix **matrices = mallocate(MATRICES_LEN, sizeof(void *));

  // init
  for (int i = 0; i < MATRICES_LEN; i++)
  {
    matrices[i] = NULL;
  }

  return matrices;
}

void free_matrices(matrix **matrices)
{
  for (int i = 0; i < MATRICES_LEN; i++)
  {
    free_matrix(matrices[i]);
  }
  free(matrices);
}

// COMPUTATION - ACCORDING TO LINE //

// note that one letter and one operation already in memory
matrix *get_result(matrix **matrices, int operation, int letter, int *error)
{

  matrix *mat_a = copy_matrix(matrices[get_mat_index(letter)]);

  // read next letter
  letter = getchar();
  if (!(letter >= 'A' || letter <= 'Z'))
  {
    *error = ERROR_BAD_INP;
    free(mat_a);
    return NULL;
  }

  matrix *mat_b = copy_matrix(matrices[get_mat_index(letter)]);

  int ab_operation;
  int bc_operation;

  if ((ab_operation = decide_oper(operation)) == ERROR_BAD_INP)
  {
    *error = ERROR_BAD_INP;
    free(mat_a);
    free(mat_b);
    return NULL;
  }

  // reads matrices one by one, until EOF reached.
  while (1)
  {

    operation = getchar();
    if (operation == '\n' || operation == EOF)
    {
      // end of computation
      break;
    }
    else if ((bc_operation = decide_oper(operation)) == ERROR_BAD_INP)
    {
      *error = ERROR_BAD_INP;
      free(mat_a);
      free(mat_b);
      return NULL;
    }

    // read next letter
    letter = getchar();
    if (!(letter >= 'A' || letter <= 'Z'))
    {
      free(mat_a);
      free(mat_b);
      *error = ERROR_BAD_INP;
      return NULL;
    }

    matrix *mat_c = copy_matrix(matrices[get_mat_index(letter)]);

    if (mat_c == NULL)
    {
      free_matrix(mat_a);
      free_matrix(mat_b);
      *error = ERROR_BAD_INP;
      return NULL;
    }

    // if newly-find operation is multiply, we multiply matrices B and C,
    // if it is another operation, we do the operation between A and B
    if (bc_operation == MULTIPLY)
    {
      mat_b = compute_matrix(mat_b, mat_c, bc_operation);
      if (mat_b == NULL)
      {
        *error = ERROR_BAD_INP;
        return NULL;
      }
    }
    else
    {
      mat_a = compute_matrix(mat_a, mat_b, ab_operation);
      if (mat_a == NULL)
      {
        *error = ERROR_BAD_INP;
        return NULL;
      }
      ab_operation = bc_operation;
      mat_b = mat_c;
    }
  }

  mat_a = compute_matrix(mat_a, mat_b, ab_operation);
  if (mat_a == NULL)
  {
    *error = ERROR_BAD_INP;
    return NULL;
  }

  return mat_a;
}

// COMPUTATION - three matrices //

int decide_oper(int operation)
{ /**
   *  asci:43 + = 0
   *  asci:45 - = 1
   *  asci:42  * = 2
   */
  switch (operation)
  {
  case 43:
    return PLUS;
    break;
  case 45:
    return MINUS;
    break;
  case 42:
    return MULTIPLY;
    break;
  }
  return ERROR_BAD_INP;
}

matrix *add_matrix(matrix *mat1, matrix *mat2, bool subtract)
{
  /**
   * @brief adds two matrices together, result is stored in memory of mat1,
   * mat2 will be freed after the computation
   *
   */
  if (mat1->cols != mat2->cols || mat1->rows != mat2->rows)
  {
    free_matrix(mat1);
    free_matrix(mat2);
    return NULL;
  }

  for (int i = 0; i < mat1->cols * mat1->rows; i++)
  {
    if (subtract)
    {
      mat1->values[i] = mat1->values[i] - mat2->values[i];
    }
    else
    {
      mat1->values[i] = mat1->values[i] + mat2->values[i];
    }
  }

  free_matrix(mat2);
  return mat1;
}

matrix *multiply_matrix(matrix *mat1, matrix *mat2)
{
  /**
   * @brief multiplies two matrices, allocates space for new matrix, frees the two
   *
   */
  if (mat1->cols != mat2->rows)
  {
    free_matrix(mat1);
    free_matrix(mat2);
    return NULL;
  }

  matrix *result = allocate_matrix(mat2->cols, mat1->rows);
  result->values = mallocate(mat1->rows * mat2->cols, sizeof(int));
  result->rows = mat1->rows;
  result->cols = mat2->cols;

  for (int r = 0; r < result->rows; r++)
  {
    for (int c = 0; c < result->cols; c++)
    {
      result->values[r * result->cols + c] = 0;
      for (int k = 0; k < mat1->cols; k++)
      {
        result->values[r * result->cols + c] += mat1->values[r * mat1->cols + k] * mat2->values[k * mat2->cols + c];
      }
    }
  }

  free_matrix(mat1);
  free_matrix(mat2);
  return result;
}

matrix *compute_matrix(matrix *mat_a, matrix *mat_b, int operation)
{ /**
   * @brief decides and performs operation between two matrices
   *
   */
  switch (operation)
  {
  case PLUS:
    mat_a = add_matrix(mat_a, mat_b, false);
    break;
  case MINUS:
    mat_a = add_matrix(mat_a, mat_b, true);
    break;
  case MULTIPLY:
    mat_a = multiply_matrix(mat_a, mat_b);
    break;
  default:
    return NULL;
  }

  if (mat_a == NULL)
  {
    return NULL;
  }

  return mat_a;
}
