
#include "matrix.h"

/* The main program */
int main(int argc, char *argv[])
{
  int error = ERROR_OK;
  matrix **matrices = create_list_of_matrices();

  while (!read_line(matrices, &error))
  {
  }

  if (error == ERROR_OK)
  {
  }
  else
  {
    fprintf(stderr, "%s", MEM_ERROR_MESSAGE);
  }

  free_matrices(matrices);
  return error;
}
