#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define BASIC_ARR_LEN 20
#define MEM_ERROR_MESSAGE "Error: Chyba paměti!\n"
#define BAD_INP_ERROR_MESSAGE "Error: Chybny vstup!\n"
#define PLUS 0
#define MINUS 1
#define MULTIPLY 2
#define START_LEN_VALUE 1
#define MATRICES_LEN 26

// error codes inicialization
enum error
{
  ERROR_OK = EXIT_SUCCESS,
  ERROR_BAD_INP = 100,
  ERROR_MEMORY,
};

typedef struct matrix
{
  int rows;
  int cols;
  int *values;
} matrix;

// reading
bool read_row(int **values, int *cols, int rows, int *error);
matrix *read_matrix(int *error);
int read_operation(void);
bool read_line(matrix **matrices, int *error);

// utils
void print_matrix(const matrix *const m);
void print_matrices(matrix **matrices);
matrix *copy_matrix(matrix *m);
int get_mat_index(int letter);

// compute
matrix *multiply_matrix(matrix *mat1, matrix *mat2);
matrix *add_matrix(matrix *m1, matrix *m2, bool subtract);
matrix *compute_matrix(matrix *mat1, matrix *mat2, int operation);
int decide_oper(int operation);
matrix *get_result(matrix **matrices, int oper, int letter, int *error);

// memory
matrix *allocate_matrix(int cols, int rows);
void *mallocate(size_t num_items, size_t size);
int *reallocate(int *values, int old_len, int new_len);
void free_matrix(matrix *m);

// list of matrices handling
matrix **create_list_of_matrices(void);
void free_matrices(matrix **matrices);
