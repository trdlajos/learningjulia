#include <stdio.h>
#include <stdlib.h>



struct QUEUE {
    struct homework *first; // pointer to first element
    struct homework *last; // pointer to last element
    int size; // count of elements in queue
}; 

/**
 * Initialize queue.
 * @return: initialized queue
 */
struct QUEUE init_queue(int size){
    struct QUEUE queue_1;
    queue_1.size=size;
    return queue_1;

}

/**
 * Prints queue q
 * @param q: queue to print
 */
void print_queue(struct QUEUE *q){
    while ((q->first))
    {
        /* code */
    }
    
}

/**
 * Pushes element to queue
 * @param q: queue
 * @param username: student username
 * @param hwname: homework name
 * @param points: points awarded for homework
 */
void push_queue(struct QUEUE *q, char *username, char *hwname, double points);

/**
 * Checks if queue q is empty
 * @param q: queue
 * @return: 1 if empty, 0 otherwise
 */
int is_empty(struct QUEUE *q);

/**
 * Pops element from queue.
 * @param q: queue
 * @return: element from queue.
 */
struct homework pop_queue(struct QUEUE *q);

/**
 * Frees queue.
 * @param q: queue
 */
void free_queue(struct QUEUE *q);


int main(int argc, char const *argv[])
{
    // TODO implement queue
    return 0;
}
