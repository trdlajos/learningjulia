#include <iostream>
using namespace std;
using num = int;

int main()
{
    num five = 5; 
    cout << five << endl;
    num &funf = five;
    cout << funf << endl;
    cout << &funf << " : "<< &five << endl;
    return 0;
}
