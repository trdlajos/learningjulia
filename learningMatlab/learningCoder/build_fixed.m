% Load the test data
load euclidean_data.mat
%% example for generation of mex file - fixed input
% example data has to be loaded!
fprintf("\nMatlab script test\n")
run("euclidean_test.m")
fprintf("\nMEX script test\n")
codegen euclidean.m -args {x,cb} -test euclidean_test

%% example of generating fixed input C code - fixed input
codegen -report -config:lib euclidean.m -args {x, cb}
% -report tells coder that we want report of translation as well
% -config:lib tells coder that we want C code as library instead of
% standart MEX - can be used to tell coder that we want MEX/C++ 
