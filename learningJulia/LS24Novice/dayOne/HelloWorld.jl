file_path = "z1.txt"
lines = readlines(file_path)

function safe_parse(arg1::String)
    # Attempt to parse the strings as integers using tryparse
    num1 = tryparse(Int, arg1)
    
    # Check if parsing was successful
    if num1 === nothing
        println("Error: One or both inputs are not valid integers.")
        exit(1)
    else
        return num1
    end
end

desired_node = safe_parse(ARGS[1])

mutable struct node
    id::Int
    conn::Vector
end

function print_strings(lines)
    for line in lines
        println(line)
    end
end

function create_node_dict(lines)
    nodes = Dict()
    for line in lines
        conn_nodes = split(line)
        add_conn!(nodes,parse(Int,conn_nodes[1]),parse(Int,conn_nodes[2]))
        end
    return nodes
end


function add_conn!(nodes,node_id_1,node_id_2)
    if !haskey(nodes,node_id_1)
        nodes[node_id_1] = node(node_id_1, [node_id_2])
    else
        push!(nodes[node_id_1].conn,node_id_2)
    end 
    if !haskey(nodes,node_id_2)
        nodes[node_id_2] = node(node_id_2, [node_id_1])
    else
        push!(nodes[node_id_2].conn,node_id_1)
    end
end

print_strings(lines)
nodes_dict = create_node_dict(lines)
println(nodes_dict[desired_node].conn)