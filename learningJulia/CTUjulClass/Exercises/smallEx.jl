# 02
    # Use for or while loop to print all integers between 1 and 100 which can be divided by both 3 and 7.

        function findNumbers(n)
            res = Vector(Int64);
            for i in 1:n
                if i % 7 == 0 && i % 3 == 0
                    append!(res,i)
                end 
            end
            @show res
            return res
        end

    # Use nested loops to create a matrix with elements given by the formula (can be found on the web)

        function populateMatrix()
            A = Matrix{Int64,2}(0,3,4)
            @show A
            for i in 1:3
                @show i
                i+=1
                for j in 1:4
                    @show j
                    
                    j+=1
                end
            end
        end

    # Factorial with recursion 

        function fact(n)
            if !isinteger(n) || n < 0
                error("Bad parameter!")
            end

            if n == 0 || n == 1
                return 1
            end

            return n*fact(n-1)
        end

        print(fact(4.3)) # will trigger error
        print(fact(3))

#ex04

    # power without ^
        function power(x::Real, p::Integer)
            
            if(p>0)
                return power(x,p-1)*x
            elif(p == 1)
                return x
            else
                return 1
            end
        end

    # one line ternary check if n is even or odd
        oneLineTernary(n::Int64) = (n%2 == 0 ? ret = true : ret = false;ret)

        quadraticForm(x::Number,y::Number;a::Number = 1, b::Number = 2*a, c::Number = 3*(a+b)) = a*x^2+b*x*y+c*y^2 

    # define probability density function for gaussian distr., check that its integral is 1
        gdd(x::Real;mu::Real = 0, delta::Real = 1) = (exp(-1/2*((x-mu)/(delta))^2)/(delta*sqrt(2*pi)))

        function integrate_func(func,step,min,max)
            # performs simple numerical integration
            x_vect = min:step:max
            
            res_vect = func.(x_vect)
            return sum(res_vect) * step # needs normalization!
        end

    # wrapper that wraps round, ceil, floor

    function wrapper(x...; type = :round, kwargs...)
        if type == :ceil
            return ceil(x...; kwargs...)
        elseif type == :floor
            return floor(x...; kwargs...)
        else
            return round(x...; kwargs...)
        end
    end