
using LinearAlg

# Implementation of MSD task 01. 
# Computing the state space representation from matrices of second order model, M is type easy, so M^(-1)-K -D B reduces to -K -D B

M = [1 0 0;
     0 1 0;
     0 0 1]

D = [2 -0.1 -0.1;
    -0.1 0.2 -0.1;
    -0.1 -0.1 1.1]

K = [4 -2 0;
    -2 4 -2;
     0 -2 22]

B = [0;
     1;
     0]

I_n = Matrix{Float64}(I, 3, 3)  # Identity matrix of size 3x3
O = zeros(3,3)
A = [O I_n;-K -D]
O = zeros(3)
Bn = [O;B]

print(A)
print(Bn)