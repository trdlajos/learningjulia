using LinearAlgebra

# Implementation of projector finding in Julia for CTU Optimalization class

UV = [2 1; 1 -1; -3 1]

v = [2;0;1]

function compute_projector(U::Matrix, rejector = false::Bool)
    if(rejector)
        P = U*inv(U'*U)*U'
        I_n = Matrix{Int64}(I, size(P)[1], size(P)[2])
        return I_n-P
    end
    return U*inv(U'*U)*U'

end

print("Projection of v onto UV: ",compute_projector(UV)*v)
print("Rejection of v onto UV: ",v.-compute_projector(UV)*v)
print("Rejection of v onto UV: ",compute_projector(UV,true)*v)

X1 = [-3//5;0;-4//5;0]
X2 = [0;0;0;1]
X3 = [4//5;0;3//5;0]
X = [X1 X2 X3]

print("Projector: ", compute_projector(X))
print("Rejector: ", compute_projector(X,true))


#QR decomposition
A= [1 2 3;3 2 1;1 0 1]
Q,R = qr(A) 
print(Matrix(Q))