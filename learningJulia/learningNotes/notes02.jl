#Fast Julia prototyping - ctrl+enter - executes one line, alt+enter - executes block(function etc...) - shift+enter - executes block #...#

# function definition

# keywords - function = return = end
# :: used for type annotation - if inputted type is different, compiler will do a conversion, if possible
# semicolon separate positional with keywords arguments !!
function quadratic(x::Real; a::Real = 1, b::Real = 1, c::Real = 1)
    value = a*x^2 + b*x + c 
    deriv = 2* a*x + b

    return value, deriv
end

# If else definition

# if uses only booleans! No 1 or 0 !
# returns value of the last expression evaluated in if block
function compare(x, y)
    if x < y
        println("x is less than y")
    elseif x > y
        println("x is greater than y")
    else
        println("x is equal to y")
    end
    return
end

function compare(x, y)
    z = if x < y
        y
    else
        x
    end
    return z
end

# Ternary operators

# syntax a ? b : c - if a true, use b, else compare
function ternary(;x,y)
    println(x < y ? "x is less than y" : "x is greater than or equal to y")
end

# Short circuit x bitwise - short circuit evalueates as small number values as possible, bitwise evcaluates all 
# && || short circuit 
# | & bitwise

# loop/while

# with return type specification

function doWhile(i)::Int64 
    while i <= 5
        @show i
        i += 1
    end
    return i
end

# Macros
# @show - prints the result of the line

function doFor()
    for i in 1:5
        @show i
    end
end

# parametrized for 

function doParamFor()
    persons = Dict("Alice" => 10, "Bob" => 23, "Carla" => 14, "Daniel" => 34);
    for (name, age) in persons
        println("Hi, my name is $name and I am $age old.")
    end
end


# List comprehension

function doCompr()
    X = [0.4, 2.3, 4.6]
    Y = [1.4, -3.1, 2.4, 5.2]
    A = Float64[exp((x^2 - y^2)/2)/2 for x in X, y in Y]
end

# Generators 

# note that () are used instead of []!
gen = (n for n in 1:10);
sum(gen)
collect(gen) # ... converting onto an vectors
gen = ((i,j) for i in 1:3 for j in 1:i if i+j == 4); # can contain if!

# Iterators

# enumerate - [i, A[i]]
# eachcol/eachrow - goes over cols/rows
# zip - creates an iterator from inputted variables 

function zipExemp()
    for (i, j, k) in zip([1, 4, 2, 5], 2:12, (:a, :b, :c))
        @show (i, j, k)
    end
end

#Scope of variables

#behavior little different in REPL and Code block - for is treaded as block that works only with local - simply use different names for each variable and you are clear :))





