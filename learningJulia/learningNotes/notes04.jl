# functions - can be one line
    g(x) = (x -= 1; x *= 2; x) # one line syntax, executed ; by ;

# optional arguments can depend on themselfs, provided correct order
    optPowers(x, y = x*x, z = y*x, v = z*x) = x, y, z, v

# keyword arguments use ; for separation!
    keyPower(x; y = x*x, z = y*x, v = z*x) = x, y, z, v
    keyPower(2, z = 10)

# multiple number of args func - ... dots after var
    nargs(x...) = println("Number of arguments: ", length(x))
    # note that it can be used to unpact values directly ...
    nargs(1:10)
    nargs(1:10...)

# h2 act as a pointer to function declared with -> 
    h2 = x ->  x^2 + 2x - 1

# map and dot examples - maps function on every variable 
    h3 = x -> x^2 + 2x - 1
    vec = [3,2,1,0]
    h3.(vec)
    map(h3,vec)

# dot or broadcast can be used for multiple input functions
    h4 = (x,y) -> x+y
    vec2 = [1,1,1,1]
    h4.(vec,vec2)
    broadcast(h4,vec,vec2)
    h4.(vec,vec2)-broadcast(h4,vec,vec2)

    # example with row vector
    rowv = [1 2 3 4]
    broadcast(h4,vec2,rowv) #performs broadcast, but shapes differ - creates matrix!
    h4.(vec2,rowv)
    h4.(vec2,rowv) - broadcast(h4,vec2,rowv)

    # piping demonstration - can be used to cahin functions - propages output of one function to input of next
    h4.(vec,vec2) .|>  h3 # here pipe |> is combined with . for broadcasting 
    h4.(vec,vec2) .|>  h3 |> sum # multiple pipes

#Methods and functions
    # function consist of methods, each implementing desired abilities for often different inputs
    # another methods of product function can be defined by specification of input arguments
    product(x::Number, y::Number) = x * y
    methods(product)
    product(x,y) = throw(ArgumentError("Product is defined only for numbers!"))
    methods(product)

    # redefining product for all variables
    product(x,y) = x*y
    methods(product)
    product("a","a") # ok
    product(:q,:x) # not ok
    
    # problems with symbols - add just definition fror strings!
    product(x::Number, y::Number) = x * y
    product(x::AbstractString,y::AbstractString) = x*y
    product(x,y) = throw(ArgumentError("Product is defined only for numbers and strings!"))
    #is there a way of determining what type of function will be called for given configuration of parameters? 
    # MACRO @which !
    @which product(2, 4.5)
    product(2,4.5)

    #Note taht this is a demonstartion - use it only when neccesary to specify diferent behavior, 
    # there is also no need to specify custom error message, as the one from compiler is more informative

# These functions can be used for looking up the hierchy of types in julia:
    # take a look how function overloading is demonstrated here! As sometimes, subtypes needs to work with vector, sometimes with type only!
    using InteractiveUtils: supertype
    function supertypes_tree(T::Type, level::Int = 0)
        isequal(T, Any) && return
        println(repeat("   ", level), T)
        supertypes_tree(supertype(T), level + 1)
        return
    end
    
    using InteractiveUtils: subtypes
    function subtypes_tree(T::Vector{Any}, level::Int = 0)
        if length(T) == 0
            return 
        end
        for type in T
            println(repeat("   ", level), type)
            subtypes_tree(subtypes(type), level + 1)
        end
        return
    end

    function subtypes_tree(T::Vector{Type}, level::Int = 0)
        if length(T) == 0
            return 
        end
        for type in T
            println(repeat("   ", level), type)
            subtypes_tree(subtypes(type), level + 1)
        end
        return
    end

    function subtypes_tree(T::Type, level::Int = 0)
        println(repeat("   ", level), T)
        subtypes_tree(subtypes(T), level + 1)
        return
    end

# Custom types
    # new abstract type
    abstract type Student end

    # defining subtypes to type student
    struct Master <: Student
        salary
    end

    struct Doctoral <: Student
        salary
        exam_mid::Bool
        exam_english::Bool
    end

    subtypes_tree(Student)

    green = Master(500)
    aver = Doctoral(4000, 0, 1)
    nerd = Doctoral(4000, 1, 1)
    lempl = Doctoral(4000, 0, 0)
    # functions manipulating custom types
    function monthlySalary(s::Master)
        return s.salary
    end
    function monthlySalary(s::Doctoral)
        bonus = 0
        s.exam_english ? bonus = bonus + 1000 : bonus
        s.exam_mid ? bonus = bonus + 2000 : bonus
        
        return s.salary + bonus
    end
    
    function studentSalary(s::Student)
        return monthlySalary(s)*12
    end 
    studentSalary(lempl)
    studentSalary(nerd)
    studentSalary(green)
    
#variables Scope
    #Orgpad

    module A
    a = 10
    b = 50
    end
    
    module B
        using Main.A:b
        using Main.A:a
    c = a+b
    end

    module B
        #problem! 
        c = a+b
    end

    A.a
    B.c

# Exception handling
    error("This thrower an ErrorExcpetion!")
    #but, there exist predefined error types to further tell developer what causes the problem!
    arg = 10
    throw(DomainError(arg,"throw throws Error given by argument! Sometimes, these error types can use additional argument together with sentence specificating error"))
    #stacktrace ... provide us with ordered list of function calls that lead us to error