using LinearAlgebra # adding packages

#VSCODE run a line - ctrl enter

# fractions can be created via // - real type!
    y = 1//2 # .. 0.5

    x = 1252.1518

    z = floor(x)

# variables and their representation, special operations

    a = 654
    b = 255
    l = 45.5

    bitstring(a)

    typeof(a)
    isinteger(a)

    typemax(Int64)
    typemin(Int64)

    eps(Float32) # minimal difference between two valid float type numbers

    div(a,b) # whole number difision

    # this can be used for looking up the hierchy of types
    using InteractiveUtils: supertype
    function supertypes_tree(T::Type, level::Int = 0)
        isequal(T, Any) && return
        println(repeat("   ", level), T)
        supertypes_tree(supertype(T), level + 1)
        return
    end

    # this can be used to show all subtypes of specific type
    using InteractiveUtils: subtypes
    function subtypes_tree(T::Vector{Any}, level::Int = 0)
        if length(T) == 0
            return 
        end
        for type in T
            println(repeat("   ", level), type)
            subtypes_tree(subtypes(type), level + 1)
        end
        return
    end

    function subtypes_tree(T::Type, level::Int = 0)
        println(repeat("   ", level), T)
        subtypes_tree(subtypes(T), level + 1)
        return
    end

    # need compare two floats if they are close enough - .\approx
    f1 = 1-1/(2)^61
    f2 = 1-1/(2)^60
    f1≈f2

# arrays, vectors, ranges
v = [1,2,3,4,5,6,7,8]

    # dot operations, can be used with = or functions as well! function_name.(x_vect) returns vector corresponding to x inputs
        (v.+4).*2

    # indexing
        v[begin:4]
        v[2:end]

    #transpose 
        v'

    # utils func 
        sizeof(v)   # in bits?
        size(v)
        ndims(v)
        eltype(v)
        length(v)
        append!(v,100)

    # range definition   start:step:stop
        r = 1:2:10
        collect(r)

#Creating a matrix 

    # classic 
        O = zeros(Int64,3,4)
        ONE = ones(Int64,3,4)
        I_n = Matrix{Int64}(I, 2, 2) # identity via LinearAlgebra packages

    # general - uninitialized Matrix/Array - make sure you populate it!
        M = Matrix{Int64}(undef,5,5)
        A =  Array{Union{Missing, Int}}(missing, 2, 3) # with missing var, so it can be checked if it was init!

    # cat =, dims specify axis, lowest dim - columns
        L = zeros(Int64,3,4)
        L[3,begin:end].=1
        L
        K = zeros(Int64,3,4)
        LK = cat(L,K, dims=2)

    # view - referencing subarray of larger array, does not make copy!
        Ll = @view LK[1:3, 1:4]
        Ll == L

    # sum, dims
        sum(L, dims = 1) # sum of columns
        sum(L, dims = 2) # sum of rows

# Tuples

    T = (1,3,4,"ds")
    a, b, c, s = T
    s
    T = (a = 1, b = 3, c = 4, s = "ds")
    T.a

    q = "Qq"
    T = [q]

# Disct 
    d = Dict("a"=>[1,2,3], :sym => [1,1])
    d["a"]
    d[:sym] # : is used for declaring symbols

    # get function]
        def_value = 10
        get(d, :sym, def_value) # return def value if sym not in dict as key, get! will add default value to dict if it does not exist