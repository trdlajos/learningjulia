# Learning Code
Repo for storage of materials I created as part of learning languages. As I learn how the thing is done in certain language, I develop my notes that can be useful for me in future. 
# Inside
* **learningJulia** - created as part of Julia course at CTU, has HW done during course
* **learningPython** - created during various parts and classes I undergo at CTU - makes use of ipython and numpy!
* **learningMatlab** - created during Automated control course at CTU as well as during my work on extending Voxel Tower
* **learningC** - created during as part of Programming course at CTU, extended during my work on Voxel Tower, has HW done during course  

