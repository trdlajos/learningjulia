import numpy as np
import IPython as ip
import math

"""
t ... test variable
NOTE array[row,columns]! --> array[new_D,...,3D,2D,1D]
"""
# definition of array
t_zeros = np.zeros((5,5))
t_classic = np.array([72, 35, 64, 88, 51, 90, 74, 12])
t_arange = np.arange(32)
# -1 in reshape specifies that numpy should come up with that value himself
t_linspace = np.linspace(10,100,10,dtype=int).reshape(2,-1)
t_transpose = t_linspace.T

# axis example
"""
NOTE axis is important! axis specifies along with axis is command performed! Creates a vector by putting : on the specified axis! 
Output will be multidimensional vector in which indexes correspond to an index of vector created, order of indexes is perserved

Example: np.max(array[x,y,z], axis = 1)[1,0] will output maximum value of vector array[1,:,0] 
note that axis = 0 is the most higher dimension!

axis = none - vector/matrix will be treated as one dimensinal

"""
temperature = np.array([72, 35, 64, 88, 100, 90, 74, 12])
t_shape = np.shape(temperature)
temperature_2D = temperature.reshape(2,4)
temperature_3D = temperature.reshape(2,2,2)
# max example
overall_max = np.max(temperature_3D, axis = None)
max_axe_ = np.max(temperature_3D, axis=1)
max_test = np.max(temperature_3D[1,:,0])
# sort example
line_sort_temp = np.sort(temperature_3D,axis = 2) 
block_sort_temp = np.sort(temperature_3D,axis = 0)
# indexing
part_vector = temperature_2D[1,1:3]


#Broadcasting
A = np.arange(32).reshape(4, 1, 8)
B = np.arange(48).reshape(1, 6, 8)
C = A+B
# Broadcast can be performed as long as matrices are same along all dimensions, or the size of dimension is 1 (in that case this dimension is extended by copying as many times as size of second matrix dictates)


#masking, filtering
numbers = np.linspace(5, 50, 24, dtype=int).reshape(4, -1)
# | or & and - used here to connect rule for making mask!
mask = (numbers % 10 == 0) | (numbers == 5)
numbers_tens_fives = numbers[mask]


# concatenating
a = np.array([[4, 8],[6, 1]])
b = np.array([[6, 5],[3, 8]])
conc_end_of_axis_0 = np.concatenate((a, b), axis=0)
conc_end_of_axis_1 = np.concatenate((a, b), axis=1)

# matrix and vector operations
ab_element_wise = a*b # or use multiply(a,b)
ab_matrix_wise = np.matmul(a,b)
v = a[0, :]
w = a[1, :]
# NOTE behaves little strangely, take loot in documentation for more info
vw_dot = np.dot(v,w)
vect_prod_a = np.cross(v, w)
vect_prod_b = np.cross(w, v)


#vectorization of methods
fac = np.vectorize(math.factorial)
factorized_a = fac(a)


#Types
"""
NOTE Numpy array has to have precisely defined type:
Integer 	64 	    int 	    np.int_
Booleans 	8 	    bool 	    np.bool_
Float 	    64 	    float 	    np.float_
Complex 	128 	complex 	np.complex_

dtype=...
str is possible, but can have problems, numpy will always size arrays to the highest str
writing into already defined array of strings a string that is longer will result in its truncation

"""
#structured array
data = np.array([
        ("joe", 32, 600000),
        ("beyonce", 38, 9001),
        ("felipe", 80, 100),
        ("mary", 15, 20),
    ], dtype=[("name", str, 10), ("age", int), ("power", int)])
data_adults = data[data["age"] > 20]
power_sorted_data = np.sort(data_adults, order="power")

# testing
ip.embed()

