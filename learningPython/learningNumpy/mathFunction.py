import numpy as np
import IPython as ip

#classic trigonometry
# for getting to know an angle - arctan2 is the best, needs both sin and cosine
scale = 1/2
angle = np.pi*scale

trig = np.array([np.sin(angle),np.cos(angle),np.arctan2(np.sin(angle),np.cos(angle))])
ip.embed()